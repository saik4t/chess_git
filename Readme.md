**Chess project**

Complete chess project developed in Unity and C#. From authentication to multiplayer mode, Fischer 960 gameplay, UI design,
are included in this project. 

---
![picture](chess.png)
