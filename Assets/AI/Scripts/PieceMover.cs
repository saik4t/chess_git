﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PieceMover : MonoBehaviour {

	/// <summary>
	/// Reference.
	/// </summary>
	public LuxChess2D manager;

	/// <summary>
	/// Square index from 0-63.
	/// </summary>
	public int index;

	/// <summary>
	/// Reference to the moving piece.
	/// </summary>
	private Transform piece;

	/// <summary>
	/// Are we dragging the piece?
	/// </summary>
	private bool isDragging = false;

	public void Update(){
		if(isDragging){
			//piece.transform.position = Input.mousePosition;
		}


	}


	public void OnMouseDownEvent() {

		if (manager.selectedPiece==null) {
			//Check if there is a piece on this position
			if (manager.GetPieceAt (index)>0 && manager.GetPieceAt (index)<=6) {
			
				//No longer parented to the square
				piece = transform.GetChild (0);
				piece.SetParent (transform.parent, true);
				piece.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(Screen.width/8,Screen.width/8);  
				//piece.localScale = new Vector3(.5f,.5f,.5f);
				manager.childPiece = piece;
				manager.childPiece.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(Screen.width/8,Screen.width/8);  
				//manager.childPiece.localScale = new Vector3(.5f,.5f,.5f);
				//We are dragging from here
				manager.DragingFrom = index;
				isDragging = true;

			
				manager.selectedPiece = transform.gameObject;
				//manager.selectedPiece.transform.localScale = new Vector3(1,1,1);
				manager.playerSelectIndicator(index);
//				GameObject go = Instantiate(manager.selectedIndicator.gameObject);
//				go.transform.SetParent(manager.selectedPiece.transform,false);
//				go.transform.localPosition = Vector3.zero;
				//print("selected at  "+manager.DragingFrom.ToString() + ""+ "index = =" +manager.GetPieceAt (index) );
			}

		
		}

		else  {

			if(manager.GetPieceAt (index)>0 && manager.GetPieceAt (index)<=6) {
				manager.childPiece.SetParent(manager.selectedPiece.transform,false);
				manager.childPiece.transform.localPosition = Vector3.zero;
				manager.selectedPiece = transform.gameObject;
				piece = transform.GetChild (0);
				piece.SetParent (transform.parent, true);
				manager.childPiece = piece;
				manager.DragingFrom = index;
				manager.playerSelectIndicator(index);
			}

			else {


				int x = 7-Mathf.Abs((int)(((Screen.width - Input.mousePosition.x) / (Screen.width/8)))); //From top down to the bottom //Left to right
				int y =  Mathf.Abs((int)(((manager.SquaresObj[0].position.y+Screen.width/16 - Input.mousePosition.y) / (Screen.width/8)))); //From top down to the bottom
				
							
				//Get index
				int myindex = x + y * 8;
				//print(manager.DragingFrom.ToString()+myindex.ToString());
				
				//Try to make the move
				if (manager.TryToMove(manager.DragingFrom, myindex))
				{
					//We made the move
					print("moved at   "+x.ToString()+y.ToString()+ "         index"+myindex.ToString());
					MovePiece(manager.SquaresObj[myindex]); //Update the piece position
					manager.SquaresObj[manager.DragingFrom].gameObject.GetComponent<Image>().color = Color.white;
					manager.UpdateBoard(); //Update the board in case of catpures, en passant or promotion
				
				}
				else
					ReturnPiece(); //Return piece to the original square
				
				manager.selectedPiece =null;
			}
		

		}


		//if(manager.selectedPiece==null)print("NULL");else print(manager.selectedPiece.name);
	}



//	public void OnMouseUpEvent() {
//
//		//Only if we are dragging
//		if (!isDragging)
//			return;
//
//		//Try to get index at which square this peice got
//		int x = Mathf.Abs(Mathf.RoundToInt((-224 - piece.transform.localPosition.x) / 64)); //Left to right
//		int y = 7 - Mathf.Abs(Mathf.RoundToInt((-224 - piece.transform.localPosition.y) / 64)); //From top down to the bottom
//
//		//Get index
//		int myindex = x + y * 8;
//
//		//Try to make the move
//		if (manager.TryToMove(manager.DragingFrom, myindex))
//		{
//			//We made the move
//
//			MovePiece(manager.SquaresObj[myindex]); //Update the piece position
//			manager.UpdateBoard(); //Update the board in case of catpures, en passant or promotion
//
//		}
//		else
//			ReturnPiece(); //Return piece to the original square
//
//		//We are not dragging anymore
//		manager.DragingFrom = -1;
//		isDragging = false;
//		print("mouse Up   "+x.ToString()+y.ToString());
//	}
//
	/// <summary>
	/// Returns piece to the original square.
	/// </summary>
	private void ReturnPiece() {
		manager.childPiece.SetParent(manager.SquaresObj[manager.DragingFrom], false);
		manager.childPiece.localPosition = Vector3.zero;
	}

	/// <summary>
	/// Moves the piece to the new square.
	/// </summary>
	private void MovePiece(Transform to) {

		var t = to.GetChild(0); //Get the piece from the square we are trying to move the piece to
		if (t) { //If there is a piece

			print("this is t"+ t.parent.transform.position);

			//Switch it
			//EACH SQUARE MUST HAVE PIECE AS A CHILD
			t.SetParent(manager.selectedPiece.transform, true);
			t.localPosition = Vector3.zero;
			print("this is t"+ t.name);
		}


		//Our piece that was moved should also have it parent changed
		manager.childPiece.SetParent(to, true);
		manager.childPiece.localPosition = Vector3.zero;

	}
}
