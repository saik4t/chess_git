﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scaling : MonoBehaviour {
	Vector3 parentScale = new Vector3(1,1,1);
	// Use this for initialization
	void Start () {
		parentScale = transform.parent.localScale;
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.parent.CompareTag("square")){
			transform.localScale = new Vector3 (1,1,1);

		}
		else transform.localScale = parentScale;
	}
}
