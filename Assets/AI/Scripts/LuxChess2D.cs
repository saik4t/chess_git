﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Text.RegularExpressions;

public class LuxChess2D : Engine {
	
	public int[] numbersDD = new int[64];
    /// <summary>
    /// Pieces set.
    /// </summary>
    [Serializable]
    public class PieceSet
    {
        public Sprite Pawn;
        public Sprite Bishop;
        public Sprite Knight;
        public Sprite Rook;
        public Sprite Queen;
        public Sprite King;
        public Sprite Square;
    }
	public static int SelectedPrefabSetInt ;
    /// <summary>
    /// White pieces.
    /// </summary>
    public PieceSet WhitePieces;

    /// <summary>
    /// Black pieces.
    /// </summary>
    public PieceSet BlackPieces;

	//SET2 Prefab
	public PieceSet WhitePieces2; 
	public PieceSet BlackPieces2;

	//set3 Prefab
	public PieceSet WhitePieces3; 
	public PieceSet BlackPieces3;

	public float xPos=-244f,yPos = 204f,squareSize = 100f;
    [HideInInspector]
    public Transform[] SquaresObj; //From 0-to 63
    public Transform CanvasTr; //Squares should be parented to the canvas
    public GameObject SquareObj; //Square prefab
    public Sprite NoPieceSprite; //If there is no piece then use this sprite for the piece image - should be transparent

    /// <summary>
    /// Comuputer thinking time. He will 'force' stop after this time.
    /// </summary>
    public int ComputerThinkingTime = 6;

    /// <summary>
    /// Starting FEn.
    /// </summary>
    public string StartingFen = FEN.Default;

	public GameObject selectedPiece;
	public Transform childPiece;

    [HideInInspector]
    public int DragingFrom; //We are dragging piece from this position

    public UnityEvent OnWhiteTurn; //Event
    public UnityEvent OnBlackTurn; //Event

    //UI
    public Text GameStateUI;
    public InputField FENOutputUI;



    private void Start() {
		SelectedPrefabSetInt = PlayerPrefs.GetInt("chessmanPrefabInt",1);
        DragingFrom = -1; //Not dragging from any square
        SquaresObj = new Transform[64]; //Inti squares

        for (int i = 0; i < 64; i++)
        {
            int x= i%8;
            int y = i/8;

            //Create square
            GameObject obj = Instantiate(SquareObj, Vector3.zero, Quaternion.identity) as GameObject;
            obj.transform.SetParent(CanvasTr, true); //set parent
			obj.transform.localPosition = new Vector3(xPos + squareSize* x, yPos - squareSize * y, 0); //Position it
			obj.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width/8,Screen.width/8);

            PieceMover pm = obj.GetComponent<PieceMover>(); //Get piece component
            pm.index = i; //Set index 
            pm.manager = this; //Set manager reference

            if ((i + y) % 2 == 0) //Change sprites according to squares
            {
				if(SelectedPrefabSetInt==1)
                	obj.GetComponent<Image>().sprite = WhitePieces.Square; //White square
				else if(SelectedPrefabSetInt==2)
					obj.GetComponent<Image>().sprite = WhitePieces2.Square; //White square
				else if(SelectedPrefabSetInt==3)
					obj.GetComponent<Image>().sprite = WhitePieces3.Square; //White square
            }
			else {
				if(SelectedPrefabSetInt==1)
					obj.GetComponent<Image>().sprite = BlackPieces.Square; //Black square
				else if(SelectedPrefabSetInt==2)
					obj.GetComponent<Image>().sprite = BlackPieces2.Square; //Black square
				else if(SelectedPrefabSetInt==3)
					obj.GetComponent<Image>().sprite = BlackPieces3.Square; //Black square
			}
              

            //Save a square in a squares array at right index
            SquaresObj[i] = obj.transform;
        }

        //Calls the init in the engine class
        InitChess(StartingFen);
        
        //Updates the board changes
        UpdateBoard();

    }

    /// <summary>
    /// Updates all pieces based on board pieces.
    /// </summary>
    public void UpdateBoard() {
        //Initialize grid

        for (int i = 0; i < 64; i++)
        {
            int piece = GetPieceAt(i); //Index of piece

			numbersDD[i] = piece;

			if (SelectedPrefabSetInt==1) {
				switch (piece) {
				//Button will shrink texture, which creates much better 'piece' effect
				case Defs.WPawn:
					CreatePiece (WhitePieces.Pawn, i);
					break;
				case Defs.WBishop:
					CreatePiece (WhitePieces.Bishop, i);
					break;
				case Defs.WKnight:
					CreatePiece (WhitePieces.Knight, i);
					break;
				case Defs.WRook:
					CreatePiece (WhitePieces.Rook, i);
					break;
				case Defs.WQueen:
					CreatePiece (WhitePieces.Queen, i);
					break;
				case Defs.WKing:
					CreatePiece (WhitePieces.King, i);
					break;
				case Defs.BPawn:
					CreatePiece (BlackPieces.Pawn, i);
					break;
				case Defs.BBishop:
					CreatePiece (BlackPieces.Bishop, i);
					break;
				case Defs.BKnight:
					CreatePiece (BlackPieces.Knight, i);
					break;
				case Defs.BRook:
					CreatePiece (BlackPieces.Rook, i);
					break;
				case Defs.BQueen:
					CreatePiece (BlackPieces.Queen, i);
					break;
				case Defs.BKing:
					CreatePiece (BlackPieces.King, i);
					break;
				case Defs.Empty:
					CreatePiece (NoPieceSprite, i);
					break;
				}
			}
			else if (SelectedPrefabSetInt==2) {
				switch (piece) {
				//Button will shrink texture, which creates much better 'piece' effect
				case Defs.WPawn:
					CreatePiece (WhitePieces2.Pawn, i);
					break;
				case Defs.WBishop:
					CreatePiece (WhitePieces2.Bishop, i);
					break;
				case Defs.WKnight:
					CreatePiece (WhitePieces2.Knight, i);
					break;
				case Defs.WRook:
					CreatePiece (WhitePieces2.Rook, i);
					break;
				case Defs.WQueen:
					CreatePiece (WhitePieces2.Queen, i);
					break;
				case Defs.WKing:
					CreatePiece (WhitePieces.King, i);
					break;
				case Defs.BPawn:
					CreatePiece (BlackPieces2.Pawn, i);
					break;
				case Defs.BBishop:
					CreatePiece (BlackPieces2.Bishop, i);
					break;
				case Defs.BKnight:
					CreatePiece (BlackPieces2.Knight, i);
					break;
				case Defs.BRook:
					CreatePiece (BlackPieces2.Rook, i);
					break;
				case Defs.BQueen:
					CreatePiece (BlackPieces2.Queen, i);
					break;
				case Defs.BKing:
					CreatePiece (BlackPieces2.King, i);
					break;
				case Defs.Empty:
					CreatePiece (NoPieceSprite, i);
					break;
				}
			}
			else if (SelectedPrefabSetInt==3) {
				switch (piece) {
				//Button will shrink texture, which creates much better 'piece' effect
				case Defs.WPawn:
					CreatePiece (WhitePieces3.Pawn, i);
					break;
				case Defs.WBishop:
					CreatePiece (WhitePieces3.Bishop, i);
					break;
				case Defs.WKnight:
					CreatePiece (WhitePieces3.Knight, i);
					break;
				case Defs.WRook:
					CreatePiece (WhitePieces3.Rook, i);
					break;
				case Defs.WQueen:
					CreatePiece (WhitePieces3.Queen, i);
					break;
				case Defs.WKing:
					CreatePiece (WhitePieces3.King, i);
					break;
				case Defs.BPawn:
					CreatePiece (BlackPieces3.Pawn, i);
					break;
				case Defs.BBishop:
					CreatePiece (BlackPieces3.Bishop, i);
					break;
				case Defs.BKnight:
					CreatePiece (BlackPieces3.Knight, i);
					break;
				case Defs.BRook:
					CreatePiece (BlackPieces3.Rook, i);
					break;
				case Defs.BQueen:
					CreatePiece (BlackPieces3.Queen, i);
					break;
				case Defs.BKing:
					CreatePiece (BlackPieces3.King, i);
					break;
				case Defs.Empty:
					CreatePiece (NoPieceSprite, i);
					break;
				}
			}

        }

    
    }
	public void SaveGame(){
		PlayerPrefsX.SetIntArray("Numbers",numbersDD);
		PlayerPrefs.SetString("lastFen",GetFen());
	}

	public void LoadGame(){
		Irreversible();
		numbersDD = PlayerPrefsX.GetIntArray("Numbers");

		InitChess(PlayerPrefs.GetString("lastFen"));
		for (int i = 0; i < 64; i++)
		{
			

			//numbersDD[i] = piece;

			if (SelectedPrefabSetInt==1) {
				switch (numbersDD [i]) {
				//Button will shrink texture, which creates much better 'piece' effect
				case Defs.WPawn:
					CreatePiece (WhitePieces.Pawn, i);
					break;
				case Defs.WBishop:
					CreatePiece (WhitePieces.Bishop, i);
					break;
				case Defs.WKnight:
					CreatePiece (WhitePieces.Knight, i);
					break;
				case Defs.WRook:
					CreatePiece (WhitePieces.Rook, i);
					break;
				case Defs.WQueen:
					CreatePiece (WhitePieces.Queen, i);
					break;
				case Defs.WKing:
					CreatePiece (WhitePieces.King, i);
					break;
				case Defs.BPawn:
					CreatePiece (BlackPieces.Pawn, i);
					break;
				case Defs.BBishop:
					CreatePiece (BlackPieces.Bishop, i);
					break;
				case Defs.BKnight:
					CreatePiece (BlackPieces.Knight, i);
					break;
				case Defs.BRook:
					CreatePiece (BlackPieces.Rook, i);
					break;
				case Defs.BQueen:
					CreatePiece (BlackPieces.Queen, i);
					break;
				case Defs.BKing:
					CreatePiece (BlackPieces.King, i);
					break;
				case Defs.Empty:
					CreatePiece (NoPieceSprite, i);
					break;
				}

				 }
			else if (SelectedPrefabSetInt==2) {
				switch (numbersDD [i]) {
				//Button will shrink texture, which creates much better 'piece' effect
				case Defs.WPawn:
					CreatePiece (WhitePieces2.Pawn, i);
					break;
				case Defs.WBishop:
					CreatePiece (WhitePieces2.Bishop, i);
					break;
				case Defs.WKnight:
					CreatePiece (WhitePieces2.Knight, i);
					break;
				case Defs.WRook:
					CreatePiece (WhitePieces2.Rook, i);
					break;
				case Defs.WQueen:
					CreatePiece (WhitePieces2.Queen, i);
					break;
				case Defs.WKing:
					CreatePiece (WhitePieces2.King, i);
					break;
				case Defs.BPawn:
					CreatePiece (BlackPieces2.Pawn, i);
					break;
				case Defs.BBishop:
					CreatePiece (BlackPieces2.Bishop, i);
					break;
				case Defs.BKnight:
					CreatePiece (BlackPieces2.Knight, i);
					break;
				case Defs.BRook:
					CreatePiece (BlackPieces2.Rook, i);
					break;
				case Defs.BQueen:
					CreatePiece (BlackPieces2.Queen, i);
					break;
				case Defs.BKing:
					CreatePiece (BlackPieces2.King, i);
					break;
				case Defs.Empty:
					CreatePiece (NoPieceSprite, i);
					break;
				}

		}
			else if (SelectedPrefabSetInt==3) {
				switch (numbersDD [i]) {
				//Button will shrink texture, which creates much better 'piece' effect
				case Defs.WPawn:
					CreatePiece (WhitePieces3.Pawn, i);
					break;
				case Defs.WBishop:
					CreatePiece (WhitePieces3.Bishop, i);
					break;
				case Defs.WKnight:
					CreatePiece (WhitePieces3.Knight, i);
					break;
				case Defs.WRook:
					CreatePiece (WhitePieces3.Rook, i);
					break;
				case Defs.WQueen:
					CreatePiece (WhitePieces3.Queen, i);
					break;
				case Defs.WKing:
					CreatePiece (WhitePieces3.King, i);
					break;
				case Defs.BPawn:
					CreatePiece (BlackPieces3.Pawn, i);
					break;
				case Defs.BBishop:
					CreatePiece (BlackPieces3.Bishop, i);
					break;
				case Defs.BKnight:
					CreatePiece (BlackPieces3.Knight, i);
					break;
				case Defs.BRook:
					CreatePiece (BlackPieces3.Rook, i);
					break;
				case Defs.BQueen:
					CreatePiece (BlackPieces3.Queen, i);
					break;
				case Defs.BKing:
					CreatePiece (BlackPieces3.King, i);
					break;
				case Defs.Empty:
					CreatePiece (NoPieceSprite, i);
					break;
				}

			}
		}
	}

    private Squares LastSquare = Squares.None;
    public override void OnTurnSwitched() //Turn was switched
    { 

        //Who's turn?
        if (SideToPlay() == 1) //White
        {
            if (OnWhiteTurn != null)
                OnWhiteTurn.Invoke();
        }
        else {
            if (OnBlackTurn != null)
                OnBlackTurn.Invoke();
            ComputerPlay(ComputerThinkingTime); //Computer should play as black
        }

        GameStateUI.text = Regex.Replace(GameState().ToString(),"[A-Z]", " $0"); //Space before capital letter
        FENOutputUI.text = GetFen();

        //Check if in check
        Squares sq = IsInCheck();
        if (sq != Squares.None) { //If in check mark square
            LastSquare = sq;
            SquaresObj[(int)LastSquare].gameObject.GetComponent<Image>().color = new Color32(255,15,15,255);
        }
        else if (LastSquare != Squares.None) {//Remove last marked square
            SquaresObj[(int)LastSquare].gameObject.GetComponent<Image>().color = Color.white;
        }
    }



    private Vector2 origPieceSize = Vector2.zero;
	private void CreatePiece(Sprite piece, int index)                                                                                         //HERE SCALE WAS 1 , float scale = 1f
    {
        Image image = SquaresObj[index].GetChild(0).gameObject.GetComponent<Image>();
		 
        //Get the size of the first piece and store it 
        if (origPieceSize == Vector2.zero)
            origPieceSize = image.rectTransform.sizeDelta;

		image.rectTransform.sizeDelta = new Vector2(Screen.width/8,Screen.width/8);                               /// origPieceSize*scale;
        image.sprite = piece;

    }

    private Squares LastSquareFrom = Squares.None;
    private Squares LastSquareTo = Squares.None;
    public override void OnComputerPlayed(int from, int to)
    {
		
        //Mark the squares that piece traveled
        SquaresObj[(int)from].gameObject.GetComponent<Image>().color = new Color32(0, 223, 255, 255);
        SquaresObj[(int)to].gameObject.GetComponent<Image>().color = new Color32(0, 255, 44, 255);

        //Reset colors
        if ((int)LastSquareFrom != from && (int)LastSquareFrom != to && LastSquareFrom != Squares.None)
        {
            SquaresObj[(int)LastSquareFrom].gameObject.GetComponent<Image>().color = Color.white;
        }

        if ((int)LastSquareTo != to && (int)LastSquareTo != from && LastSquareTo != Squares.None)
        {
            SquaresObj[(int)LastSquareTo].gameObject.GetComponent<Image>().color = Color.white;
        }

        //Store marked squares
        LastSquareFrom = (Squares)from;
        LastSquareTo = (Squares)to;

        //Since board was changed by computer update board
        UpdateBoard();
    }

	void Update(){
		
	}
    public void Undo() {

        //Calls the engine undo move
        UndoMove();

        //Reset color
        SquaresObj[(int)LastSquareFrom].gameObject.GetComponent<Image>().color = Color.white;
        SquaresObj[(int)LastSquareTo].gameObject.GetComponent<Image>().color = Color.white;
        
        //Sinde the board has changed update the move
        UpdateBoard();
    }

	public void playerSelectIndicator(int x){
		for(int i=0;i<64;i++){
			SquaresObj[i].gameObject.GetComponent<Image>().color = Color.white;
			SquaresObj[x].gameObject.GetComponent<Image>().color = new Color32(21,0,176,255);
		}
	}



}
