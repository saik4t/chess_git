﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : Chessman {

	public override bool[,] PossibleMove()
	{
		bool[,] r = new bool[8,8];
		Chessman c,c2;
		int[] w = BoardManager.Instance.EnPassantMoveWhite;
		int[] b = BoardManager.Instance.EnPassantMoveBlack;
		//whites turn
		if(isWhite)
		{
			//DIAGONAL LEFT
			if(CurrentX!=0 && CurrentY != 7)
			{
				
				if(w[0] == CurrentX -1 && w[1] == CurrentY +1 && BoardManager.Instance.isWhiteTurn)
				{
					r[CurrentX - 1 , CurrentY + 1] = true;

				}
				c= BoardManager.Instance.Chessmans[CurrentX-1,CurrentY+1];
				if(c!=null && !c.isWhite)
					r[CurrentX-1,CurrentY+1] = true;
			}

			//DIAGONAL RIGHT

			if(CurrentX!=7 && CurrentY != 7)
			{
				
				if(w[0] == CurrentX + 1 && w[1] == CurrentY + 1 &&  BoardManager.Instance.isWhiteTurn)
				{
					r[CurrentX + 1 , CurrentY + 1] = true;

				}

				c= BoardManager.Instance.Chessmans[CurrentX+1,CurrentY+1];
				if(c!=null && !c.isWhite)
					r[CurrentX+1,CurrentY+1] = true;
			}
			//MIDDLE
			if(CurrentY!=7){
				c = BoardManager.Instance.Chessmans[CurrentX,CurrentY+1];
				if(c==null )
					r[CurrentX,CurrentY+1] = true;
			}
			//MIDDLE DOUBLE AT FIRST MOVE
			if(CurrentY==1)
			{
				c = BoardManager.Instance.Chessmans[CurrentX,CurrentY+1];
				c2 = BoardManager.Instance.Chessmans[CurrentX,CurrentY+2];

				if(c == null && c2 == null)
					r[CurrentX,CurrentY+2] = true;
			}
		}

		else 
		{	//Black pawn Movement
			//DIAGONAL LEFT
			if(CurrentX!=0 && CurrentY != 0)
			{
				if(b[0] == CurrentX -1 && b[1] == CurrentY - 1 && !BoardManager.Instance.isWhiteTurn)
				{
					r[CurrentX - 1 , CurrentY - 1] = true;

				}
				c= BoardManager.Instance.Chessmans[CurrentX-1,CurrentY-1];
				if(c!=null && c.isWhite)
					r[CurrentX-1,CurrentY-1] = true;
			}

			//DIAGONAL RIGHT

			if(CurrentX!=7 && CurrentY != 0)
			{

				if(b[0] == CurrentX + 1 && b[1] == CurrentY -1 && !BoardManager.Instance.isWhiteTurn)
				{
					r[CurrentX + 1 , CurrentY - 1] = true;

				}

				c= BoardManager.Instance.Chessmans[CurrentX+1,CurrentY-1];
				if(c!=null && c.isWhite)
					r[CurrentX+1,CurrentY-1] = true;
			}
			//MIDDLE
			if(CurrentY!=0){
				c = BoardManager.Instance.Chessmans[CurrentX,CurrentY-1];
				if(c==null )
					r[CurrentX,CurrentY-1] = true;
			}
			//MIDDLE DOUBLE AT FIRST MOVE
			if(CurrentY==6)
			{
				c = BoardManager.Instance.Chessmans[CurrentX,CurrentY-1];
				c2 = BoardManager.Instance.Chessmans[CurrentX,CurrentY-2];

				if(c == null && c2 == null)
					r[CurrentX,CurrentY-2] = true;
			}
		}
		return r;
	}
}
