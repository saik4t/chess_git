﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FenDataSetUp : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	public void Fiscer () {
		
		List<int> freeSpaceWhite = new List<int>();
		List<int> freeSpaceBlack = new List<int>();
		char[] fiscerFEN = new char[8];



		for(int i=0;i<7;i=i+2)
			freeSpaceBlack.Add(i);
		for(int i=1;i<8;i=i+2)
			freeSpaceWhite.Add(i);

		int rook1,rook2,b1,b2,king;
		rook1 = Random.Range(0,8);
		if(freeSpaceBlack.Contains(rook1))
			freeSpaceBlack.Remove(rook1);
		else if(freeSpaceWhite.Contains(rook1))
			freeSpaceWhite.Remove(rook1);


		//Spawning single rook
		//SpawnChessMan(2,rook1,0);
		//SpawnChessMan(8,rook1,7);
		fiscerFEN[rook1] = 'r';

		//Spawning Second rook
		rook2 = rook1;
		while (Mathf.Abs(rook1-rook2)<=1)
			rook2 = Random.Range(0,8);

		if(freeSpaceBlack.Contains(rook2))
			freeSpaceBlack.Remove(rook2);
		else if(freeSpaceWhite.Contains(rook2))
			freeSpaceWhite.Remove(rook2);

		//SpawnChessMan(2,rook2,0);
		//SpawnChessMan(8,rook2,7);
		fiscerFEN[rook2] = 'r';

		//Spawn King
		king = Random.Range(1,(Mathf.Abs(rook1-rook2)-1));
		if(rook1>rook2){
			//SpawnChessMan(0,rook2+king,0);
			//SpawnChessMan(6,rook2+king,7);
			fiscerFEN[rook2+king] = 'K';

			//				leftRookWhite = Chessmans[rook2,0];
			//				leftRookBlack=Chessmans[rook2,7];
			//				rightRookWhite = Chessmans[rook1,0];
			//				rightRookBlack = Chessmans[rook1,7];
			//				whiteKing = Chessmans[rook2+king,0];
			//				blackKing = Chessmans[rook2+king,7];

			if(freeSpaceBlack.Contains(rook2+king))
				freeSpaceBlack.Remove(rook2+king);
			else if(freeSpaceWhite.Contains(rook2+king))
				freeSpaceWhite.Remove(rook2+king);
		}
		else {
			fiscerFEN[rook1+king] = 'K';
			//				SpawnChessMan(0,rook1+king,0);
			//				SpawnChessMan(6,rook1+king,7);
			//
			//				leftRookWhite = Chessmans[rook1,0];
			//				leftRookBlack=Chessmans[rook1,7];
			//				rightRookWhite = Chessmans[rook2,0];
			//				rightRookBlack = Chessmans[rook2,7];
			//				whiteKing = Chessmans[rook1+king,0];
			//				blackKing = Chessmans[rook1+king,7];

			if(freeSpaceBlack.Contains(rook1+king))
				freeSpaceBlack.Remove(rook1+king);
			else if(freeSpaceWhite.Contains(rook1+king))
				freeSpaceWhite.Remove(rook1+king);
		}



		//Respawn Bishop
		int[] BW = freeSpaceWhite.ToArray();
		int[] BB = freeSpaceBlack.ToArray();
		b1 = Random.Range(0,freeSpaceWhite.Count);
		b2 =Random.Range(0,freeSpaceBlack.Count);

		fiscerFEN[BW[b1]] = 'b';
		fiscerFEN[BB[b2]] = 'b';
		//			SpawnChessMan(3,BW[b1],0);
		//			SpawnChessMan(9,BW[b1],7);
		//			SpawnChessMan(3,BB[b2],0);
		//			SpawnChessMan(9,BB[b2],7);
		freeSpaceWhite.Remove(BW[b1]);
		freeSpaceBlack.Remove(BB[b2]);

		//Respawn The REst
		freeSpaceWhite.AddRange(freeSpaceBlack);

		int[] rest3 = freeSpaceWhite.ToArray();
		//Spawn knight
		//			SpawnChessMan(4,rest3[0],0);
		//			SpawnChessMan(10,rest3[0],7);
		//			SpawnChessMan(4,rest3[2],0);
		//			SpawnChessMan(10,rest3[2],7);
		fiscerFEN[rest3[0]] = 'k';
		fiscerFEN[rest3[2]] = 'k';

		//Spawn Queen
		//			SpawnChessMan(1,rest3[1],0);
		//			SpawnChessMan(7,rest3[1],7);
		fiscerFEN[rest3[1]] = 'q';
		string c = new string(fiscerFEN);
//		for(int i=0;i<8;i++){
//
//			print(fiscerFEN[i]);
//
//		}
		print(c);
		BoardManager.FEN = c;
		getUserID.FEN_sample_ins = c;
		//Spawn white Pawns
		//			for (int i=0;i<8;i++)
		//				SpawnChessMan(5,i,1);
		//			//Black Pawns
		//			for (int i=0;i<8;i++)
		//				SpawnChessMan(11,i,6);
		//
		//			freeSpaceBlack.Clear();
		//			freeSpaceWhite.Clear();
		//
		//			//Pawn
		//			for (int i=0;i<8;i++)
		//				SpawnChessMan(5,i,1);
		//			//Pawn
		//			for (int i=0;i<8;i++)
		//				SpawnChessMan(11,i,6);
		//
		//			leftRookWhite = Chessmans[0,0];
		//			rightRookWhite = Chessmans[7,0];
		//			leftRookBlack = Chessmans[0,7];
		//			rightRookBlack = Chessmans[7,7];
		//			whiteKing = Chessmans[4,0];
		//			blackKing = Chessmans[4,7];
		//
		//			foreach(GameObject chessman in activeChessman){
		//				if(chessman.GetComponent<Chessman>().isWhite)
		//					activeChessmanWhite.Add(chessman);
		//				else activeChessmanBlack.Add(chessman);
		//			}
	}


	public void Standard (){

		string c = "rkbqKbkr";
		BoardManager.FEN = c;
		getUserID.FEN_sample_ins = c;
		print(c);
	}
	
}
