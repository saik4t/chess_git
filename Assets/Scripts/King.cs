﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class King : Chessman {
	
	public  bool castleAbleLeft = true;
	public  bool castleAbleRight = true;



	public override bool[,] PossibleMove(){
		bool[,] r = new bool[8,8];


		Chessman c;
		int i,j;


		if (isWhite) {
			if (castleAbleLeft && BoardManager.Instance.castleAbleW_L && BoardManager.Instance.isWhiteTurn && BoardManager.Instance.Chessmans[2,0]==null)
				r [2, 0] = true;
			if (castleAbleRight && BoardManager.Instance.castleAbleW_R && BoardManager.Instance.isWhiteTurn && BoardManager.Instance.Chessmans[6,0]==null)
				r [6, 0] = true;
		}
		if (!isWhite) {
			if (castleAbleLeft && BoardManager.Instance.castleableB_L && !BoardManager.Instance.isWhiteTurn && BoardManager.Instance.Chessmans[2,7]==null)
				r [2, 7] = true;
			if (castleAbleRight && BoardManager.Instance.castleableB_R && !BoardManager.Instance.isWhiteTurn && BoardManager.Instance.Chessmans[6,7]==null)
				r [6, 7] = true;
		}


		//Top side
		i = CurrentX - 1;
		j = CurrentY + 1;
		if(CurrentY != 7){

			if(CurrentX != 7 && CurrentX !=0){
			for (int k = 0; k<3;k++)
			{
				if(i>=0 || i<8){
					c = BoardManager.Instance.Chessmans[i,j];
					if(c==null)
						r[i,j] = true;
					else if(isWhite != c.isWhite)
						r[i,j] = true;
					}
					i++;
				}
			}
			else if(CurrentX ==7){
				for (int k = 0; k<2;k++)
				{
					if(i>=0 || i<8){
						c = BoardManager.Instance.Chessmans[i,j];
						if(c==null)
							r[i,j] = true;
						else if(isWhite != c.isWhite)
							r[i,j] = true;
					}
					i++;
				}
			}
			else if(CurrentX == 0){
				i = CurrentX;
				for (int k = 0; k<2;k++)
				{
					if(i>=0 || i<8){
						c = BoardManager.Instance.Chessmans[i,j];
						if(c==null)
							r[i,j] = true;
						else if(isWhite != c.isWhite)
							r[i,j] = true;
					}
					i++;
				}
			}
		} 

		//Down Side
		i = CurrentX - 1;
		j = CurrentY - 1;
		if(CurrentY!=0){
			if(CurrentX != 7 && CurrentX !=0){
				for (int k = 0; k<3;k++)
				{
					if(i>=0 || i<8){
						c = BoardManager.Instance.Chessmans[i,j];
						if(c==null)
							r[i,j] = true;
						else if(isWhite != c.isWhite)
							r[i,j] = true;
					}
					i++;
				}
			}
			else if(CurrentX ==7){
				for (int k = 0; k<2;k++)
				{
					if(i>=0 || i<8){
						c = BoardManager.Instance.Chessmans[i,j];
						if(c==null)
							r[i,j] = true;
						else if(isWhite != c.isWhite)
							r[i,j] = true;
					}
					i++;
				}
			}
			else if(CurrentX == 0){
				i = CurrentX;
				for (int k = 0; k<2;k++)
				{
					if(i>=0 || i<8){
						c = BoardManager.Instance.Chessmans[i,j];
						if(c==null)
							r[i,j] = true;
						else if(isWhite != c.isWhite)
							r[i,j] = true;
					}
					i++;
				}
			}
		} 

		//Middle left
		if(CurrentX!=0)
		{
			c = BoardManager.Instance.Chessmans[CurrentX -1,CurrentY];
			if(c==null)
				r[CurrentX-1,CurrentY] = true;
			else if (isWhite !=c.isWhite)
				r[CurrentX-1,CurrentY] = true;
		}

		//Middle Right
		if(CurrentX!=7)
		{
			c = BoardManager.Instance.Chessmans[CurrentX +1,CurrentY];
			if(c==null)
				r[CurrentX+1,CurrentY] = true;
			else if (isWhite !=c.isWhite)
				r[CurrentX+1,CurrentY] = true;
		}



		return r;
	
	}

}
