﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class getUserID : MonoBehaviour {

	public static string ownId_ins;
	public static string challengeID_ins;
	public static string opponentID_ins;
	public static int win_by_disconnect_ins;
	public static int player_StopWatch_min_ins;
	public static bool can_The_Player_Win_ins;

	public static int incomming_challenge_ins;
	public static string playerColorOwn_ins;
	public static string string_960_ins;
	public static bool am_I_a_Challenger_ins;

	public static string FEN_sample_ins;
	public string FEN_sample_INS;
	public bool am_I_a_Challenger_INS;

	public string playerColorOwn_INS;

	//profile items
	public static int rank_ins,points_ins,isPlaying_ins,avatarId_ins,matchPlayed_ins,matchWon_ins,matchDrawn_ins;
	public static string displayName_ins, country_ins;
	//profile items

	// values for inspector window
	public string ownid_INS, challengeid_INS, opponentID_INS;
	/// <summary>
	/// 0-draw;1=win;-1=lose;5=notDecided
	/// </summary>
	public int win_by_disconnect_INS; 
	public bool can_The_Player_Win_INS;
	public int player_StopWatch_min_INS;

	public int incomming_challenge_INS;
	public string string_960_INS;

	//profile items - values for inspector window
	public int rank_INS,points_INS,isPlaying_INS,avatarId_INS,matchPlayed_INS,matchWon_INS,matchDrawn_INS;
	public string displayName_INS, country_INS;
	//profile items - values for inspector window

	// Use this for initialization
	void Start () {

		isPlaying_ins = 0;

		FEN_sample_ins = FEN_sample_INS;

		win_by_disconnect_ins = 5;
		can_The_Player_Win_ins = true;
		player_StopWatch_min_ins = 0;
	}


	
	// Update is called once per frame
	void Update () {
		FEN_sample_INS = FEN_sample_ins;

		am_I_a_Challenger_INS = am_I_a_Challenger_ins;
		ownid_INS = ownId_ins;
		challengeid_INS = challengeID_ins;
		opponentID_INS = opponentID_ins;
		win_by_disconnect_INS = win_by_disconnect_ins;
		can_The_Player_Win_INS = can_The_Player_Win_ins;
		player_StopWatch_min_INS = player_StopWatch_min_ins;
		can_The_Player_Win_INS = can_The_Player_Win_ins;
		incomming_challenge_INS = incomming_challenge_ins;
		string_960_INS = string_960_ins;

		playerColorOwn_INS = playerColorOwn_ins;

		//player profile info
		rank_INS=rank_ins;
		points_INS=points_ins;
		isPlaying_INS=isPlaying_ins;
		avatarId_INS=avatarId_ins;
		matchPlayed_INS=matchPlayed_ins;
		matchWon_INS=matchWon_ins;
		matchDrawn_INS=matchDrawn_ins;
		country_INS=country_ins;
		displayName_INS = displayName_ins;

	}
}
