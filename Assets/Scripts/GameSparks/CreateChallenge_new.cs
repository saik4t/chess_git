﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using GameSparks.Api;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using UnityEngine.SceneManagement;

public class CreateChallenge_new : MonoBehaviour {

	public List<string> userIds = new List<string> {""};
	string gameType_Data;

	// Use this for initialization
	void Start () {

//		userIds.Clear();
//		userIds.Add(getUserID.opponentID_ins);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void	CreateChallenge_n(){

		getUserID.am_I_a_Challenger_ins = true;

		userIds.Clear();
		userIds.Add(getUserID.opponentID_ins);

		int minuteToPlay = stopwatch.minChoosen + 5;

		Debug.Log ("NOW : " + System.DateTime.Now);
		Debug.Log ("Date : " + System.DateTime.Now.AddDays(1) + " Time : " + System.DateTime.Now.AddMinutes(15));

		new CreateChallengeRequest()
			.SetAccessType("PRIVATE")
			.SetChallengeMessage(getUserID.FEN_sample_ins)
			.SetChallengeShortCode("chessChal")
			.SetEndTime(System.DateTime.Now.AddMinutes(minuteToPlay)) //We set a date and time the challenge will end on
			.SetMaxPlayers(2)
			.SetMinPlayers(0)
			.SetSilent(false)
			.SetUsersToChallenge(userIds)
			.Send((response) => {
				if(!response.HasErrors){
					string challengeInstanceId = response.ChallengeInstanceId; 
					GSData scriptData = response.ScriptData; 
					//challegeID = challengeInstanceId;
					Debug.Log("challenged done. Cid: " + challengeInstanceId);
					string challegeID_challenged = challengeInstanceId;

					//go to the chess board screeen and wait for 50sec for accepting challenge and start game
					//function here

					getUserID.challengeID_ins = challegeID_challenged;
					//setting global challengeID

					new LogEventRequest()
						.SetEventKey("UpdateChellengeID")
						.SetEventAttribute("challenge_id_", challegeID_challenged)
						.Send((response2) =>
							{
								if (!response2.HasErrors)
								{
									Debug.Log("challengeID Data sent - Done");
								}
								else
								{
									Debug.Log("Failed");
								}
							});

					////start the loading scene
					SceneManager.LoadScene ("4");


//					SceneManager.LoadScene ("2");


				}
				else{
					Debug.Log("can not challenge user");
				}
			});


	}
}
