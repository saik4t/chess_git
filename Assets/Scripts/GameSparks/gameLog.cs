﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using GameSparks.Api;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using UnityEngine.UI;
using System.Linq;
using System;

public class gameLog : MonoBehaviour {

	public string nextTurn, playerColor;

	public Text ShowTurn;

	public string challengeID_temp;
	public string ownId_temp, oppoentId_temp;

	//public int xc = System.Int32.Parse("5") + 5;

	public int gsfromData, gstoData, tempfromdata;
	public string  gsTurn;

	public int fx,fy,tx,ty;

	public static gameLog instance;
	public int selectedFromX,selectedFromY,movedToX,movedToY ;
	// Use this for initialization
	void Start () {

		//tempfromdata = 99;


		challengeID_temp = getUserID.challengeID_ins;
		ownId_temp = getUserID.ownId_ins;
		oppoentId_temp = getUserID.opponentID_ins;


		//		BoardManager.Instance.SelectChessMan (stringToInt_x("00"), 1);
		//		BoardManager.Instance.MoveChessMan (0, 2);
		//
		instance = this;

		turnFixer ();

		//InvokeRepeating("getData",1,2f);
	}

	void Update () {

		GameSparks.Api.Messages.ChallengeTurnTakenMessage.Listener = (message) => {
			var challenge = message.Challenge; 
			String messageId = message.MessageId; 
			bool? notification = message.Notification; 

			String nextPlayerID = challenge.NextPlayer;

			Debug.Log("c_id : " + challenge.ChallengeId);
			Debug.Log("c_msg : " + challenge.ChallengeMessage);
			Debug.Log("next player (msg) : " + challenge.NextPlayer.ToString());
			Debug.Log("message received");

			//after every turn taken its called
			//when other player takes his turn the NEXT PLAYER is ownId
			//and then only this method is called to get data

			if(nextPlayerID==ownId_temp){
				getData();
			}
				
		};

		GameSparks.Api.Messages.ChallengeWonMessage.Listener =  (message) => {
			var challenge = message.Challenge; 
			getUserID.win_by_disconnect_ins = 1;
			BoardManager.Instance.EndGame(0);
		};

		GameSparks.Api.Messages.ChallengeLostMessage.Listener =  (message) => {
			var challenge = message.Challenge; 
			getUserID.win_by_disconnect_ins = -1;
			BoardManager.Instance.EndGame(0);
		};

		GameSparks.Api.Messages.ChallengeDrawnMessage.Listener =  (message) => {
			var challenge = message.Challenge; 
			getUserID.win_by_disconnect_ins = 0;
			BoardManager.Instance.EndGame(0);
		};

		if(ownId_temp == nextTurn)
			ShowTurn.text = "YOU";
		else
			ShowTurn.text = "Opponent";


	}





	public void selectedChessPoint(int x,int y){
		selectedFromX = x;
		selectedFromY = y;
	}

	public void MovedChessPoint(int x, int y){
		movedToX = x;
		movedToY = y;
	}

	string valTostring (int a, int b){
		string valfrom = a.ToString()+b.ToString();
		return valfrom;
	}//no need

	int	valueJoiner(int a, int b){
		int aa = a * 10 + b;
		return aa;
	}


	int stringToInt_x(System.String ab){

		int a = System.Int32.Parse(ab)/10;

		Debug.Log("int a : " + a);

		return a;

	}

	int stringToInt_y(System.String ab){

		int b =  System.Int32.Parse(ab);
		b = b%10;

		Debug.Log("int b : " + b);

		return b;

	}


	public void sendData (){

		//check the time
		string timeToSend = stopwatch.currnetMinToSend.ToString();


		//check if the player is in online_players list


		//		string fromData = valTostring(selectedFromX,selectedFromY);
		//		string toData = valTostring(movedToX,movedToY);

		int fromData = valueJoiner(selectedFromX,selectedFromY);
		int toData = valueJoiner(movedToX,movedToY);
		//if(nextTurn == ownId_temp)
		new LogChallengeEventRequest().SetChallengeInstanceId(challengeID_temp)
			.SetEventKey("chessTurn") //The event we are calling is "takeTurn", we set this up on the GameSparks Portal
			.SetEventAttribute("fromPOS", fromData) //takeTurn has an attribute for position called "pos", we supply it with the pos we placed our icon at
			.SetEventAttribute("toPOS", toData) //takeTurn also has an attribute called "playerIcon, we set to our X or O
			.SetEventAttribute("turn", timeToSend)
			.Send((response) =>
				{
					if (response.HasErrors)
					{
						Debug.Log("not your turn");

					}
					else
					{
						// If our ChallengeEventRequest was successful we inform the player
						Debug.Log("data sent");
						nextTurn = getUserID.opponentID_ins; //has to be fixed//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						//chamge the turn

						////ChallengeList.turn = ChallengeList.ownID;
					}
				});

	}


	public void getData(){

		//check if the player is in online_players list
		//if player is not in the list wait for 50sec
		// check each 2sec if player is in online_player list again

		//if not player one wins

		if (nextTurn != ownId_temp) {

			new ListChallengeRequest ().SetShortCode ("chessChal")
				.SetState ("RUNNING") //We want to get all games that are running
				.SetEntryCount (50) //We want to pull in the first 50
				.Send ((response) => {
					if (!response.HasErrors) {
						//Debug.Log ("count : " + response.ChallengeInstances.Count ());
						//For every challenge we receive
						foreach (var challenge in response.ChallengeInstances) {

							if(challenge.ChallengeId == challengeID_temp){

							string nextPlaaya = challenge.NextPlayer;
							Debug.Log ("nextplayer from challenge : " + nextPlaaya);

							if (nextPlaaya == ownId_temp) {
								nextTurn = nextPlaaya; 

								gsfromData = (int)challenge.ScriptData.GetInt ("fromPOS");
								gstoData = (int)challenge.ScriptData.GetInt ("toPOS");
								gsTurn = challenge.ScriptData.GetString ("turn");

								Debug.Log ("moving datas : "+gsfromData.ToString () + " , " + gstoData.ToString() + " the data type is : "+ gsfromData.GetType ());


									MoveRemotely ();
									Debug.Log ("moved remotely");


							}
						}//if same channelge id.. do this

						}

					} else {
						Debug.Log ("error in data receiving..");
					}
				});
		}

		//		Debug.Log(gsfromData.GetType());
		//		Debug.Log(gstoData.GetType());

//		if (gsfromData != tempfromdata) {
//			Debug.Log ("tempFromData : " + tempfromdata.ToString());
//			MoveRemotely ();
//			Debug.Log ("moved remotely");
//			tempfromdata = gsfromData;
//		}




	}

	public void turnFixer(){

		new ListChallengeRequest().SetShortCode("chessChal")
			.SetState("RUNNING") //We want to get all games that are running
			.SetEntryCount(50) //We want to pull in the first 50
			.Send((response) =>
				{
					if(!response.HasErrors){
						Debug.Log("turn fixed !!");
						//For every challenge we receive
						foreach (var challenge in response.ChallengeInstances)
						{
							if(challenge.ChallengeId==getUserID.challengeID_ins){
							nextTurn = challenge.NextPlayer; 
							Debug.Log("1st turn :" + nextTurn);

							if (nextTurn == ownId_temp) {

								playerColor = "white";
								getUserID.playerColorOwn_ins = playerColor;
								BoardManager.Instance.IswhiteTeam = true;
									BoardManager.playerColor = playerColor;
							}
							else{
								playerColor = "black";
								getUserID.playerColorOwn_ins = playerColor;
								BoardManager.Instance.IswhiteTeam = false;
									BoardManager.playerColor = playerColor;
							}

							Debug.Log ("you are : " + playerColor);
						}
						}

					}
					else {
						Debug.Log("error in data receiving..");
					}

				});






	}
		

	public void turnFixer_new(){

		new ListChallengeRequest().SetShortCode("chessChal")
			.SetState("RUNNING") //We want to get all games that are running
			.SetEntryCount(50) //We want to pull in the first 50
			.Send((response) =>
				{
					if(!response.HasErrors){
						Debug.Log("turn fixed !!");
						//For every challenge we receive
						foreach (var challenge in response.ChallengeInstances)
						{
							nextTurn = challenge.NextPlayer; 
							Debug.Log("1st turn :" + nextTurn);

							if (nextTurn == ownId_temp) {

								playerColor = "white";
								BoardManager.Instance.IswhiteTeam = true;

							}
							else{
								playerColor = "black";
								BoardManager.Instance.IswhiteTeam = false;
							}

							Debug.Log ("you are : " + playerColor);
						}

					}
					else {
						Debug.Log("error in data receiving..");
					}
				});






	}


	void MoveRemotely(){
		BoardManager.Instance.SelectChessMan (gsfromData/10,gsfromData%10);
		BoardManager.Instance.MoveChessMan (gstoData/10,gstoData%10);
	}



	public void gameover_Server(){

		new LogChallengeEventRequest().SetChallengeInstanceId(challengeID_temp)
			.SetEventKey("chessTurn") //The event we are calling is "takeTurn", we set this up on the GameSparks Portal
			.SetEventAttribute("fromPOS", 0) 
			.SetEventAttribute("toPOS", 0) 
			.SetEventAttribute("turn", "won")
			.Send((response) =>
				{
					if (response.HasErrors)
					{
						Debug.Log("not your turn to FINISH GAME");

					}
					else
					{
						// If our ChallengeEventRequest was successful we inform the player
						Debug.Log("data sent !! CHECK MATE");
						//nextTurn = "match completed"; 
						//chamge the turn

						////ChallengeList.turn = ChallengeList.ownID;
					}
				});


	}

}
