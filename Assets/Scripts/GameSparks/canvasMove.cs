﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class canvasMove : MonoBehaviour {
	public GameObject cam;
	 float positionR = 0;
	 float positionL = 0;
	bool  left = true;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(left)
			cam.transform.position = Vector3.Lerp(cam.transform.position,Vector3.right*positionR,.1f);
		else 
			cam.transform.position = Vector3.Lerp(cam.transform.position,Vector3.left*positionL,.1f);
	}

	public void moveRight(){
		left = true;
		positionR = -6.39f;
	}
	public void moveLeft(){
		left = false;

	}

}
