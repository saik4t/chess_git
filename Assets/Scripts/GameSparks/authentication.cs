﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using GameSparks.Api;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.SceneManagement;

public class authentication : MonoBehaviour {
	public InputField regDisplayName,regEmail,regPass1,regPass2;
	public InputField username,password;
	public Text warning;

	public bool needAuthLogin=false;

	public static string OWNID_main;
	// Use this for initialization
	void Start () {
		warning.text = "";



		Invoke("authenticateExisting",5);

		if(needAuthLogin)
		CancelInvoke();


	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void authenticateExisting(){

		if(!needAuthLogin){
		
		new AccountDetailsRequest()
			.Send((response) => {
				if(!response.HasErrors){
				string displayName = response.DisplayName; 
					Debug.Log("welcome " + displayName);
					
				
						GSData scriptData = response.ScriptData; 

						int avatarid = (int) scriptData.GetInt("avatarid");
						int rank = (int) scriptData.GetInt("rank");
						int points = (int) scriptData.GetInt("points");
						int played = (int) scriptData.GetInt("played");
						int drawn = (int) scriptData.GetInt("drawn");
						int won = (int) scriptData.GetInt("won");
						int isplaying = (int) scriptData.GetInt("isplaying");
						string country = response.Location.Country;

						getUserID.ownId_ins = response.UserId;
						getUserID.displayName_ins = displayName;
						getUserID.avatarId_ins = avatarid;
						getUserID.rank_ins = rank;
						getUserID.points_ins = points;
						getUserID.matchPlayed_ins = played;
						getUserID.matchDrawn_ins = drawn;
						getUserID.matchWon_ins = won;
						getUserID.isPlaying_ins = isplaying;
						getUserID.country_ins = country;


						//goto next scene
					SceneManager.LoadScene("MAIN");

				}

				else{

						needAuthLogin = true;
						Debug.Log("need login");
						//SceneManager.LoadScene("1"); //go to sign in pages
					
				}
			});
		}

	}

	public void authUser(){
		new GameSparks.Api.Requests.AuthenticationRequest()
			.SetPassword(password.text)
			.SetUserName(username.text)
			.Send((response) => {

				if(!response.HasErrors){
				string authToken = response.AuthToken; 
				string displayName = response.DisplayName; 

					OWNID_main = response.UserId; 
					warning.text = "";
					Debug.Log("auth req done ============== "+"auth: "+authToken + " displayName: "+displayName + " userId: "+OWNID_main);
				
					//setting global ownID
					getUserID.ownId_ins = OWNID_main;

					//setting all profile value
					getUserAuthenticationDetails();

					SceneManager.LoadScene("MAIN");

				}


				else{

					warning.text = "Invalid username or password";
					Debug.Log("invalid pass or user");
				}
			});

	}


	public void regUser(){
		if(regPass1.text == regPass2.text){
		new GameSparks.Api.Requests.RegistrationRequest()
			.SetDisplayName(regDisplayName.text)
			.SetPassword(regPass2.text)
			.SetUserName(regEmail.text)
			.Send((response) => {
					if(!response.HasErrors){
				string authToken = response.AuthToken; 
				string displayName = response.DisplayName; 
				bool? newPlayer = response.NewPlayer; 
				GSData scriptData = response.ScriptData; 
				var switchSummary = response.SwitchSummary; 
				string userId = response.UserId; 

				Debug.Log("regDone ----------- " + "auth: "+authToken + " displayName: "+displayName + " userId: "+userId);
						//if successfull...next function run here


						//automatic sign in


					}
					else{
						Debug.Log("user exist! please try again.");
					}
				});
		}
					
		else Debug.Log("Password dows not match ");




	}


	public void getUserAuthenticationDetails(){
	
		new AccountDetailsRequest()
			.Send((response) => {
				if(!response.HasErrors){
					string displayName = response.DisplayName; 
					Debug.Log("welcome " + displayName);


					GSData scriptData = response.ScriptData; 

					int avatarid = (int) scriptData.GetInt("avatarid");
					int rank = (int) scriptData.GetInt("rank");
					int points = (int) scriptData.GetInt("points");
					int played = (int) scriptData.GetInt("played");
					int drawn = (int) scriptData.GetInt("drawn");
					int won = (int) scriptData.GetInt("won");
					int isplaying = (int) scriptData.GetInt("isplaying");
					string country = response.Location.Country;

					getUserID.ownId_ins = response.UserId;
					getUserID.displayName_ins = displayName;
					getUserID.avatarId_ins = avatarid;
					getUserID.rank_ins = rank;
					getUserID.points_ins = points;
					getUserID.matchPlayed_ins = played;
					getUserID.matchDrawn_ins = drawn;
					getUserID.matchWon_ins = won;
					getUserID.isPlaying_ins = isplaying;
					getUserID.country_ins = country;


				}

				else{
					
					Debug.Log("FAILED : auth details");

				}
			});
	}


}
