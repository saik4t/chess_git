﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Chessman : MonoBehaviour {
	public int CurrentX;
	public int CurrentY;

	public int PreviousX;
	public int PreviousY;

	public int tempX{set;get;}
	public int tempY{set;get;}


	public bool isWhite;

	public void SetPosition(int x, int y){
		CurrentX = x;
		CurrentY = y;
	}

	public void SetPreviousPosition(int x, int y){
		PreviousX = x;
		PreviousY = y;
	}

	public void tempPos(int x, int y){
		tempX = x;
		tempY = y;
	}

	public virtual bool[,] PossibleMove(){
		return new bool[8,8];
	}
}
