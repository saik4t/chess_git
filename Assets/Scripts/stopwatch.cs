﻿using UnityEngine.UI;
using UnityEngine;

public class stopwatch : MonoBehaviour {
	public Text p1s0,p1s1,p1m0,p1m1,p2s0,p2s1,p2m0,p2m1;
	public int sec1=0,min1=0,sec2=0,min2=0;
	public  float tempSec1=0,tempMin1=0,tempSec2=0,tempMin2=0;
	public static int minChoosen=10;
	public static float currnetMinToSend;

	// Use this for initialization
	void Start () {
		resetTime();
		p1s1.text="0";
		p2s1.text = "0";
	}
	
	// Update is called once per frame
	void Update () {
		//Player1 timer
		if(BoardManager.Instance.isWhiteTurn){
			if (!BoardManager.Instance.checkmate) {
				tempSec1 -= Time.deltaTime;
				tempMin1 -= Time.deltaTime;
			}
			}
		sec1 = (int)tempSec1;
		min1 = (int)tempMin1/60;
		if(sec1<0){
			tempSec1=59;
		}


		p1s0.text = (sec1 % 10).ToString ();
		if (sec1 != minChoosen*60)
		p1s1.text = ((tempSec1/60-min1)*60 / 10).ToString ();
		p1m0.text = (min1 % 10).ToString (); //OwngetUserID.player_StopWatch_min_ins
		p1m1.text = (min1 / 10).ToString ();


		//Player2 timer
		if(!BoardManager.Instance.isWhiteTurn){
			if (!BoardManager.Instance.checkmate) {
				tempSec2 -= Time.deltaTime;
				tempMin2 -= Time.deltaTime;
			}
			}
		sec2 = (int)tempSec2;
		min2 = (int)tempMin2/60;
		if(sec2==tempSec2){
			p2s1.text="0";
		}
		if(sec2<0){
			sec2=59;
		}

		p2s0.text = (sec2%10).ToString();
		if(sec2!=minChoosen*60)
		p2s1.text = ((tempSec2/60-min2)*60 / 10).ToString ();
		p2m0.text = (min2%10).ToString();
		p2m1.text = (min2/10).ToString();

		currnetMinToSend = minutePassed_2calculate();


		if(tempSec1<=0)
		{	
			tempSec1 = 0;
			tempMin1 = 0;
			BoardManager.Instance.EndGame(11);
		}
		else if(tempSec2<=0)
		{
			tempSec2 = 0;
			tempMin2 = 0;
			BoardManager.Instance.EndGame(-11);
		}
	}

	public void resetTime(){
		tempSec1=minChoosen*60;
		tempMin1=tempSec1;
		tempSec2=tempSec1;
		tempMin2=tempSec1;
	}
	public float minutePassed_2calculate(){
		if (BoardManager.onlineMultiplayer) {
			if (getUserID.playerColorOwn_ins == "white") {
				return minChoosen - (sec1 / 60);
			} else if (getUserID.playerColorOwn_ins == "black") {
				return minChoosen - (sec2 / 60);
			}
		}
		return (minChoosen-(sec1/60));
	}

}
