﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardHighLight : MonoBehaviour {
	public static BoardHighLight Instance{set;get;}

	public GameObject highLightPrefab,checkHighlight,castleButtonWhiteLeft,castleButtonBlackLeft,castleButtonWhiteRight,castleButtonBlackRight;
	private List<GameObject> highLights;
	private List<GameObject> highLightsForCheck = new List<GameObject>();

	void Start(){
		Instance = this;
		highLights = new List<GameObject>();
		castleButtonWhiteLeft.SetActive(false);
		castleButtonWhiteRight.SetActive(false);
		castleButtonBlackLeft.SetActive(false);
		castleButtonBlackRight.SetActive(false);

	}

	//This is for normal move
	GameObject getHighLightObject(){
		GameObject go = highLights.Find(g => !g.activeSelf);

		if(go == null)
		{
			go = Instantiate(highLightPrefab);
			highLights.Add(go);
		}
		return go;
	}

	//This is for Check_checkCondition move
	GameObject getHighLightObjectforCheck(){
		GameObject go = highLightsForCheck.Find(g => !g.activeSelf);

		if(go == null)
		{
			go = Instantiate(checkHighlight);
			highLightsForCheck.Add(go);
		}
		return go;
	}

	public void HighlightButtonCastleWhiteLeft(){
		castleButtonWhiteLeft.SetActive(true);
		castleButtonWhiteLeft.transform.position = Camera.main.WorldToScreenPoint(BoardManager.Instance.whiteKing.transform.position+new Vector3(-1f,-1f,0f));

	}
	public void HighlightButtonCastleWhiteRight(){
		castleButtonWhiteRight.SetActive(true);
		castleButtonWhiteRight.transform.position = Camera.main.WorldToScreenPoint(BoardManager.Instance.whiteKing.transform.position+new Vector3(1f,-1f,0f));

	}
	public void HighlightButtonCastleBlackLeft(){
		castleButtonBlackLeft.SetActive(true);
		castleButtonBlackLeft.transform.position = Camera.main.WorldToScreenPoint(BoardManager.Instance.blackKing.transform.position+new Vector3(-1f,1f,0f));

	}
	public void HighlightButtonCastleBlackRight(){
		castleButtonBlackRight.SetActive(true);
		castleButtonBlackRight.transform.position = Camera.main.WorldToScreenPoint(BoardManager.Instance.blackKing.transform.position+new Vector3(1f,1f,0f));

	}

	//highlight for possible move
	public void HighLightAllowedMoves(bool [,] moves){
		for(int i=0;i<8;i++){
			for(int j=0;j<8;j++){
				if (moves[i,j]){
					GameObject go = getHighLightObject();
					go.SetActive(true);
					go.transform.position = new Vector3 (i+.5f,j+.5f,1);
				}
			}
		}
	}

	//highlight for next check possibility after a move move
	public void HighLightCheckMoves(bool [,] moves){
		for(int i=0;i<8;i++){
			for(int j=0;j<8;j++){
				if (moves[i,j]){
					GameObject go = getHighLightObjectforCheck();
					go.SetActive(true);
					go.transform.position = new Vector3 (i+.5f,j+.5f,1);
				}
			}
		}
	}

	public void HideHighLight()
	{
		if(highLights.Count>0){
		foreach (GameObject go in highLights)
			go.SetActive(false);
		}
		castleButtonWhiteLeft.SetActive(false);
		castleButtonWhiteRight.SetActive(false);
		castleButtonBlackLeft.SetActive(false);
		castleButtonBlackRight.SetActive(false);

	}

	public void HideHighLightCheckCondition()
	{
		foreach (GameObject go in highLightsForCheck)
			go.SetActive(false);
	}
}
