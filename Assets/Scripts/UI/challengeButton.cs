﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class challengeButton : MonoBehaviour {
	public TextMeshProUGUI challnegeInt;
	public GameObject IntImage,ChallangePanel;
	bool clicked = false;
	// Use this for initialization
	void Start () {
		challnegeInt = IntImage.GetComponentInChildren<TextMeshProUGUI>();
		IntImage.SetActive(false);
		//ChallangePanel.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if(getUserID.incomming_challenge_ins<1)
			ChallangePanel.SetActive(false);
		
		challnegeInt.SetText(getUserID.incomming_challenge_ins.ToString());
		if(getUserID.incomming_challenge_ins>0){
			IntImage.SetActive(true);
		}
		else IntImage.SetActive(false);
	}

	public void  onClicked(){
		
		clicked=!clicked;
		if(clicked &&getUserID.incomming_challenge_ins>0 )
			ChallangePanel.SetActive(true);
		else ChallangePanel.SetActive(false);
	}
}
