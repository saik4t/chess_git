﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class navdrawer : MonoBehaviour {
	
	public GameObject navDrawPanel,onlinePlayerPanel,NavdrawB,onlinePlayerIm1,onlinePlayerIm2;
	public RectTransform navDrawButton,challangeAcceptButton;
	public float testdistance = 0,PlayerTestDistance=0,smoothfactor =1f,speedPlayer=1f,SmoothRotation = 4;
	int i=0,j=0;
	private Vector3 velocity = Vector3.zero;
	Vector3 primaryPos,endPos,primaryPosPlayer,endPosPlayer;
	// Use this for initialization
	void Start () {
		testdistance = Screen.width/5*3;
		PlayerTestDistance = Screen.height*0.81f;
		primaryPos = navDrawPanel.transform.position;
		endPos = new Vector3(primaryPos.x+testdistance,primaryPos.y);
		navDrawButton.position = new Vector3(navDrawButton.position.x,Screen.height*0.94f,0);
		challangeAcceptButton.position = new Vector3(challangeAcceptButton.position.x,Screen.height*0.94f,0);
		primaryPosPlayer = onlinePlayerPanel.transform.position;
		endPosPlayer =  new Vector3(primaryPosPlayer.x,primaryPosPlayer.y+PlayerTestDistance);
		//navDrawPanel.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(navDrawPanel.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x,Screen.height);
		//Debug.Log(navDrawPanel.transform.GetChild(0).name);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
		Quaternion endTarget = Quaternion.Euler(0, 0, 90);
		Quaternion endTarget2 = Quaternion.Euler(0, 0, -90);
		Quaternion startTarget = Quaternion.Euler(0, 0, 0);

		if(i%2!=0){
			navDrawPanel.SetActive(true);
			navDrawPanel.transform.position = Vector3.SmoothDamp(navDrawPanel.transform.position,endPos,ref velocity,smoothfactor);

			NavdrawB.transform.rotation = Quaternion.Slerp(NavdrawB.transform.rotation, endTarget, Time.deltaTime * SmoothRotation);

		}
		else if(i%2==0){
			Invoke("navdrawDisable",smoothfactor);
			navDrawPanel.transform.position = Vector3.SmoothDamp(navDrawPanel.transform.position,primaryPos,ref velocity,smoothfactor);
			NavdrawB.transform.rotation = Quaternion.Slerp(NavdrawB.transform.rotation, startTarget, Time.deltaTime * SmoothRotation);

		}

		if(j%2!=0){
			
			onlinePlayerPanel.transform.position = Vector3.SmoothDamp(onlinePlayerPanel.transform.position,endPosPlayer,ref velocity,smoothfactor);
			onlinePlayerIm1.transform.rotation = Quaternion.Slerp(onlinePlayerIm1.transform.rotation, endTarget2, Time.deltaTime * SmoothRotation);
			onlinePlayerIm2.transform.rotation = Quaternion.Slerp(onlinePlayerIm2.transform.rotation, endTarget, Time.deltaTime * SmoothRotation);
		}
		else if(j%2==0){
			
			onlinePlayerPanel.transform.position = Vector3.SmoothDamp(onlinePlayerPanel.transform.position,primaryPosPlayer,ref velocity,smoothfactor);
			onlinePlayerIm1.transform.rotation = Quaternion.Slerp(onlinePlayerIm1.transform.rotation, startTarget, Time.deltaTime * SmoothRotation);
			onlinePlayerIm2.transform.rotation = Quaternion.Slerp(onlinePlayerIm2.transform.rotation, startTarget, Time.deltaTime * SmoothRotation);
		}
	}

	public void navExpand(){
		i++;
		//print(i.ToString()+j.ToString());
	}

	public void onlinePlayerExpand(){
		j++;
		//print(i.ToString()+j.ToString());
	}
	void navdrawDisable(){
		navDrawPanel.SetActive(false);
	}



}
