﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Settings : MonoBehaviour {
	/// <summary>
	/// chessman Prfab according to figure 
	/// </summary>
	public static int chessmanPrefabInt = 1;
	public GameObject[] Set1Prefab= new GameObject[12];
	public GameObject[] Set2Prefab= new GameObject[12];
	public GameObject[] Set3Prefab= new GameObject[12];
	public static GameObject[] activeChessmanPrefab = new GameObject[12];
	// Use this for initialization
	void Start(){
		int i = PlayerPrefs.GetInt("chessmanPrefabInt",1);
		if(i==1)
			chessMnaPrefabSelected1();
		else if(i==2)
			chessMnaPrefabSelected2();
		else if(i==3)
			chessMnaPrefabSelected3();
			
	}
	public void chessMnaPrefabSelected1(){
		PlayerPrefs.SetInt("chessmanPrefabInt",1);
		chessmanPrefabInt = 1;
		LuxChess2D.SelectedPrefabSetInt=1;
		transform.GetChild(0).GetComponent<Image>().color = new Color32(144,255,120,255);
		transform.GetChild(1).GetComponent<Image>().color = new Color32(255,255,255,255);
		transform.GetChild(2).GetComponent<Image>().color = new Color32(255,255,255,255);
		for(int i=0;i<12;i++){
			activeChessmanPrefab[i] = Set1Prefab[i];
		}
	}

	public void chessMnaPrefabSelected2(){
		PlayerPrefs.SetInt("chessmanPrefabInt",2);
		chessmanPrefabInt = 2;
		LuxChess2D.SelectedPrefabSetInt=2;
		transform.GetChild(0).GetComponent<Image>().color = new Color32(255,255,255,255);
		transform.GetChild(1).GetComponent<Image>().color = new Color32(144,255,120,255);
		transform.GetChild(2).GetComponent<Image>().color = new Color32(255,255,255,255);
		for(int i=0;i<12;i++){
			activeChessmanPrefab[i] = Set2Prefab[i];
		}
	}

	public void chessMnaPrefabSelected3(){
		PlayerPrefs.SetInt("chessmanPrefabInt",3);
		chessmanPrefabInt = 3;
		LuxChess2D.SelectedPrefabSetInt=3;
		transform.GetChild(0).GetComponent<Image>().color = new Color32(255,255,255,255);
		transform.GetChild(1).GetComponent<Image>().color = new Color32(255,255,255,255);
		transform.GetChild(2).GetComponent<Image>().color = new Color32(144,255,120,255);
		for(int i=0;i<12;i++){
			activeChessmanPrefab[i] = Set3Prefab[i];
		}
	}

}
