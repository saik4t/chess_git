﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class fiscerUI : MonoBehaviour {
	
 bool standardGame = true;


	void Update(){
		if(standardGame){
			transform.GetChild(0).GetComponent<Image>().color = new Color32(118,101,83,255);
			transform.GetChild(1).GetComponent<Image>().color = new Color32(174,128,78,255);
		}
		else {
			transform.GetChild(0).GetComponent<Image>().color = new Color32(174,128,78,255);
			transform.GetChild(1).GetComponent<Image>().color = new Color32(118,101,83,255);
		}
	}
	
	public void FiscerClicked(){
		standardGame = false;
		print(transform.GetChild(0).name);
		print(transform.GetChild(1).name);

	}
	public void StandardClicked(){
		standardGame = true;
	}

}
