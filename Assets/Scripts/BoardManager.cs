﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
[RequireComponent(typeof(Chessman))]
public class BoardManager : MonoBehaviour {
	//saving player color
	public static string playerColor;
	[HideInInspector]


	public Text notificationCheck_W,notificationCheck_B,notification;
	public bool castleAbleW_R = false,castleableB_R = false,castleAbleW_L = false,castleableB_L = false;
	//Selecting OnlineMultiplayer Or Offline Multiplayer
	public static bool onlineMultiplayer = false;
	public static BoardManager Instance{set;get;}

	private bool[,] allowedMoves{set;get;}
	private bool[,] thiscreatesCheck{set;get;}
	private bool[,] check_checkCondition{set;get;}
	private bool[,] DefendCheck{set;get;}

	public GameObject Indicator;
	public Chessman[,] Chessmans{set;get;}
	public Chessman SelectedChessMan;
	public Chessman CapturedChessman,leftRookWhite,rightRookWhite,leftRookBlack,rightRookBlack,whiteKing,blackKing;

	public List<GameObject> chessmanPrefab ;
	public List<GameObject> activeChessman ;
	public List<GameObject> activeChessmanWhite ;
	public List<GameObject> activeChessmanBlack ;
	public GameObject Camera_ ;

	const float TILE_SIZE = 1f;
	const float TILE_OFFSET = .5f;
	[HideInInspector]
	public bool tempCheck = true,checkmate = false;

	int selectionX = -1,selectionY= -1;
	public bool isWhiteTurn = true,isWhiteCheck=false,IsBlackCheck = false,IswhiteTeam;
	public bool whitePlayerMayWin_boardmanager=true,blackPlayerMayWin_boardmanager=true;
	public int[] EnPassantMoveWhite{set;get;} 
	public int[] EnPassantMoveBlack{set;get;}

	public static string FEN =  "rkbqKbkr";
	void Start(){

		//to check the color of a static value only
		getUserID.string_960_ins = playerColor;

		Camera_ = GameObject.FindGameObjectWithTag("MainCamera");
		//getting player color

		activeChessman = new List<GameObject>();
		activeChessmanWhite = new List<GameObject>();
		activeChessmanBlack = new List<GameObject>();
		Chessmans = new Chessman[8,8];
		EnPassantMoveWhite = new int[2]{-1,-1};
		EnPassantMoveBlack = new int[2]{-1,-1};

		//Fiscer960SetUp();
		Invoke("RotateBoard",1f);
		Indicator.SetActive(false);
		Instance = this;
		notificationCheck_W.text = "";
		notificationCheck_B.text = "";
		notification.text = "";
	}



	void Update(){
		UpdateSelection();
		DrawChessBoard();

	

		//Selects or moves a chessman
		if(Input.GetMouseButtonDown(0)){
			if(selectionX>=0 &&selectionY>=0){
				if(SelectedChessMan==null)
				{
					SelectChessMan(selectionX,selectionY);
				}
				else
				{
					MoveChessMan(selectionX,selectionY);
				}
			}
		}
	}

	//selects a chessman
	public  void SelectChessMan(int x, int y)
	{	
		//Detects if the click is valid ie there is actually a chessman where it is clicked
		if(Chessmans[x,y] ==null)
			return;

		//if the selection is valid ie u cannot select black white it is whites turn
		if(Chessmans[x,y].isWhite != isWhiteTurn)
			return;
		//		if(isWhiteTurn!=IswhiteTeam)
		//			return;
		//Selects the Chessman
		SelectedChessMan = Chessmans[x,y];
		if(SelectedChessMan.GetType()==typeof(King)){
			print("king selected");
			if(SelectedChessMan.isWhite){
				print("white king selected");
				if(castleAbleW_L && SelectedChessMan.GetComponent<King>().castleAbleLeft){
					if (SelectedChessMan.CurrentX==1 ||  SelectedChessMan.CurrentX==2 ||  SelectedChessMan.CurrentX==3 || leftRookWhite.CurrentX==2)
					{
						BoardHighLight.Instance.HighlightButtonCastleWhiteLeft();
						print(" button should pop up");
					}
				}
				if(castleAbleW_R && SelectedChessMan.GetComponent<King>().castleAbleRight ){
					if (SelectedChessMan.CurrentX==5 ||   rightRookWhite.CurrentX==6)
					{
						BoardHighLight.Instance.HighlightButtonCastleWhiteRight();
						print(" button should pop up");
					}
				}
			}
			else {
				print("Black king selected");
				if(castleableB_L && SelectedChessMan.GetComponent<King>().castleAbleRight ){
					if (SelectedChessMan.CurrentX==1 ||  SelectedChessMan.CurrentX==2 ||  SelectedChessMan.CurrentX==3 || rightRookBlack.CurrentX==2)
						BoardHighLight.Instance.HighlightButtonCastleBlackLeft();
				}
				if(castleableB_R && SelectedChessMan.GetComponent<King>().castleAbleLeft ){
					if (SelectedChessMan.CurrentX==5 ||   rightRookWhite.CurrentX==6)
						BoardHighLight.Instance.HighlightButtonCastleBlackRight();
				}
			}
		}
		//allowed move is true for possible move squares
		allowedMoves = Chessmans[x,y].PossibleMove();
	
		//This allowes not to select if there is no possible move
		bool hasAtLeastOneMove = false;

		for(int i=0;i<8;i++)
			for (int j = 0; j<8;j++)
				if(allowedMoves[i,j])
					hasAtLeastOneMove = true;
		if(!hasAtLeastOneMove)
			return;




		//Clear all proneTocheck list so that in a new move everything is new
		CapturedChessman = null;
		//Upon selection highlights the possible move
		BoardHighLight.Instance.HighLightAllowedMoves(allowedMoves);
		print("selected "+x.ToString()+"  "+y.ToString()+SelectedChessMan.name);
		///
		///Pop up castle Button
		/// 

		if(onlineMultiplayer)
			gameLog.instance.selectedChessPoint(x,y);

		//Hide check Conditions
		//BoardHighLight.Instance.HideHighLightCheckCondition();
	}

	//Moves the chessman after selection
	public  void MoveChessMan(int x, int y){

		if(allowedMoves[x,y]){

			if(Chessmans[x,y]!=null)
			{
				CapturedChessman = Chessmans[x,y];
			}



			//Capture a piece and destroy it

			//c is the sqaure where chessman is supposed to move
			Chessman c =  Chessmans[x,y];
			if(c != null && c.isWhite != isWhiteTurn)
			{


				//Removes chessman since it is captured
				activeChessman.Remove(c.gameObject);
				Destroy(c.gameObject);


			}


			if (isWhiteTurn) {
				//Destroys enpassant pawn
				if (x == EnPassantMoveWhite [0] && y == EnPassantMoveWhite [1]) {

					c = Chessmans [x, y - 1];

					activeChessman.Remove (c.gameObject);
					Destroy (c.gameObject);


				}
			}

			else if (!isWhiteTurn) {
				//Destroys enpassant pawn
				if (x == EnPassantMoveBlack [0] && y == EnPassantMoveBlack [1]) {

					c = Chessmans [x, y + 1];

					activeChessman.Remove (c.gameObject);
					Destroy (c.gameObject);	

				}
			}



			//Detect and set possibility of en passant
			EnPassantMoveWhite[0] = -1;
			EnPassantMoveWhite[1] = -1;
			EnPassantMoveBlack[0] = -1;
			EnPassantMoveBlack[1] = -1;
			//Enpassant move 
			if(SelectedChessMan.GetType()==typeof(Pawn))
			{

				//en passant
				if(SelectedChessMan.CurrentY == 1 && y == 3)
				{
					EnPassantMoveBlack[0] = x;
					EnPassantMoveBlack[1] = y-1;
				}

				else if(SelectedChessMan.CurrentY == 6 && y == 4)
				{
					EnPassantMoveWhite[0] = x;
					EnPassantMoveWhite[1] = y+1;
				}

			}




			//Normal position set of chessman
			NormalMove(SelectedChessMan,x,y);




			//Check if that move is invalid ie creates self check, then it redirects all to its initial pos like nothing happened

			if(thisMoveCreatesSelfCheck()){

				GoToPreviousPos(SelectedChessMan,x,y,CapturedChessman);
				notification.text = "Invalid Move";
				return; 

			}

			//Pawn Promotion
			if(SelectedChessMan.GetType()==typeof(Pawn))
			{

				if(y==7)
				{
					activeChessman.Remove(SelectedChessMan.gameObject);
					Destroy(SelectedChessMan.gameObject);
					SpawnChessMan(1,x,y);
					activeChessman.Add(Settings.activeChessmanPrefab[1].gameObject);
					SelectedChessMan = Chessmans[x,y];
				}
				else if(y==0)
				{
					activeChessman.Remove(SelectedChessMan.gameObject);
					Destroy(SelectedChessMan.gameObject);
					SpawnChessMan(7,x,y);
					activeChessman.Add(Settings.activeChessmanPrefab[1].gameObject);
					SelectedChessMan = Chessmans[x,y];
				}
			}

			notificationCheck_W.text = "";
			notificationCheck_B.text = "";
			IsBlackCheck=false;
			isWhiteCheck = false;

			//moves a chess man visually
			SelectedChessMan.transform.position = GetTileCenter(x,y);


			//use player color here to define when to send data only..

			if (onlineMultiplayer) {
				if (isWhiteTurn == IswhiteTeam) {
					gameLog.instance.MovedChessPoint (x, y);
					gameLog.instance.sendData ();
				}
			}
			print("moved to "+x.ToString()+"  " + y.ToString());

			//Turn changed
			isWhiteTurn = ! isWhiteTurn;

			for (int a = 0; a < 8; a++) {
				for (int b = 0; b < 8; b++) {

					//Notify when there is a check
					if( Chessmans[a,b]!=null && Chessmans[a,b].isWhite!=isWhiteTurn){
						check_checkCondition = Chessmans[Chessmans[a,b].CurrentX,Chessmans[a,b].CurrentY].PossibleMove();
						//print("Selected skdksdjksd"+Chessmans[a,b].CurrentX+Chessmans[a,b].CurrentY);
					}

					//					else if( Chessmans[a,b]!=null && !Chessmans[a,b].isWhite){
					//						check_checkCondition = Chessmans[Chessmans[a,b].CurrentX,Chessmans[a,b].CurrentY].PossibleMove();
					//						print("Selected skdksdjksd"+Chessmans[a,b].CurrentX+Chessmans[a,b].CurrentY);
					//					}
					//BoardHighLight.Instance.HighLightCheckMoves(check_checkCondition);

					for(int i=0;i<8;i++){
						for (int j = 0;j<8;j++){

							if(check_checkCondition[i,j] && Chessmans[i,j]!=null)
							{
								//Create 2d array like highlightCheckMove(),add the possible chessman to a list, check if there is a king in the list

								if(Chessmans[i,j].GetType() == typeof(King)){
									if(!Chessmans[i,j].isWhite) 
									{
										notificationCheck_W.text = "Black Check";
										notificationCheck_W.color = Color.black;
										print("Black check");
										IsBlackCheck = true;
										BoardHighLight.Instance.HideHighLight();
									}
									if(Chessmans[i,j].isWhite)
									{
										notificationCheck_B.text = "White Check";
										notificationCheck_B.color = Color.white;
										print("white check");
										isWhiteCheck = true;
										BoardHighLight.Instance.HideHighLight();
									}

								}


							}

						}
					}
				}

			}

			if(isWhiteCheck || IsBlackCheck){
				thisIsCheckMate();
				if(checkmate){
					notification.text="CheckMate!!";

					Invoke("EndGame2",3);
					Invoke("EndGame2",6);
					//					EndGame(); //end game
				}
				//return;
			}

			notification.text = "";

			//castleMove that moves rook
			MoveRookWhileCAstling(x,y);
			//CAstling Possible only once
			MakeNotCastleAble();

			activeChessmanWhite.Clear();
			activeChessmanBlack.Clear();
			foreach(GameObject chessman in activeChessman){
				if(chessman.GetComponent<Chessman>().isWhite)
					activeChessmanWhite.Add(chessman);
				else activeChessmanBlack.Add(chessman);
			}

		}
		//after a move all highylights are disabled
		BoardHighLight.Instance.HideHighLight();
		//after all chessman info is provided to the square,give room to selectedchessman for another selection



		SelectedChessMan = null;




		if(thereIsNoChessmanOrCheckBetweenWhiteTeamLeft(whiteKing,0)){
			castleAbleW_L = true;
		}
		else castleAbleW_L = false;
		if(thereIsNoChessmanOrCheckBetweenWhiteTeamRight(whiteKing,0)){
			castleAbleW_R = true;
		}
		else castleAbleW_R = false;
		if(thereIsNoChessmanOrCheckBetweenBlackTeamLeft(blackKing,7)){
			castleableB_L = true;
		}
		else  castleableB_L = false;
		if(thereIsNoChessmanOrCheckBetweenBlackTeamRight(blackKing,7)){
			castleableB_R = true;
		}
		else castleableB_R = false;


	}

	void NormalMove(Chessman z,int x,int y){



		//position is set for previous position in setprevious func
		z.SetPreviousPosition(z.CurrentX,z.CurrentY);

		//The initial square from where it is moves is now null	
		Chessmans[z.CurrentX,z.CurrentY] =null;

		//position is set for new position in setposition func
		z.SetPosition(x,y);

		//Current square now represents the new chessman after its move
		Chessmans[x,y] = z;





	}

	void GoToPreviousPos(Chessman z,int x, int y,Chessman tempCap){

		//reassign value
		Chessmans[z.PreviousX,z.PreviousY] =z;

		//reassign value
		z.SetPosition(z.PreviousX,z.PreviousY);

		if(tempCap!=null){
			Chessmans[x,y] = tempCap;
			GameObject g ;
			if(onlineMultiplayer && playerColor=="black")
				g =  Instantiate(tempCap.gameObject,GetTileCenter(x,y),Quaternion.Euler(new Vector3(0,0,180))) as GameObject;
			else  g =  Instantiate(tempCap.gameObject,GetTileCenter(x,y),Quaternion.identity) as GameObject;
			Chessmans[x,y] = g.GetComponent<Chessman>();
			activeChessman.Add(g.gameObject);

		}
		else Chessmans[x,y] = null;
	}

	//Selects a chessman by clicking
	void UpdateSelection(){
		if(!Camera.main)
			return;
		RaycastHit hit;
		if (onlineMultiplayer) {
			if (isWhiteTurn == IswhiteTeam && Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit, 25f, LayerMask.GetMask ("ChessBoard"))) {
				selectionX = (int)hit.point.x;
				selectionY = (int)hit.point.y;

			} else {
				selectionX = -1;
				selectionY = -1;
			}
		}
		else{
			if ( Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit, 25f, LayerMask.GetMask ("ChessBoard"))) {
				selectionX = (int)hit.point.x;
				selectionY = (int)hit.point.y;

			} else {
				selectionX = -1;
				selectionY = -1;
			}
		}
	}


	void DrawChessBoard(){
		Vector2 widthLine = Vector2.right * 8;
		Vector2 heightLine = Vector2.up * 8;

		for(int i = 0;i<=8;i++){
			Vector2 start = Vector2.up*i;
			Debug.DrawLine(start,start+ widthLine);
			for(int j = 0;j<=8;j++){
				start = Vector2.right*i;
				Debug.DrawLine(start,start+ heightLine);
			}
		}

		//Indicate selection
		if(selectionX>=0 && selectionY>=0){
			Indicator.SetActive(true);
			Vector2 roughWork = GetTileCenter(selectionX,selectionY);  //This is to set indicator in a 3d space not 2d
			Indicator.transform.position = new Vector3(roughWork.x,roughWork.y,1);
		}
		else Indicator.SetActive(false);
	}

	// Receieve FEN data in Start


	//Spawn Single chessman
	void SpawnChessMan(int index,int x, int y){
		if (onlineMultiplayer) {
			if (playerColor == "white") {
				GameObject gmObjct = Instantiate (Settings.activeChessmanPrefab [index], GetTileCenter (x, y), Quaternion.identity) as GameObject;
				gmObjct.transform.SetParent (transform);
				Chessmans [x, y] = gmObjct.GetComponent<Chessman> (); 
				Chessmans [x, y].SetPosition (x, y);
				activeChessman.Add (gmObjct);
			} else {
				GameObject gmObjct = Instantiate (Settings.activeChessmanPrefab [index], GetTileCenter (x, y), Quaternion.Euler (new Vector3 (0, 0, 180))) as GameObject;
				gmObjct.transform.SetParent (transform);
				Chessmans [x, y] = gmObjct.GetComponent<Chessman> (); 
				Chessmans [x, y].SetPosition (x, y);
				activeChessman.Add (gmObjct);
			}
		}

		else {
			GameObject gmObjct = Instantiate (Settings.activeChessmanPrefab [index], GetTileCenter (x, y), Quaternion.identity) as GameObject;
			gmObjct.transform.SetParent (transform);
			Chessmans [x, y] = gmObjct.GetComponent<Chessman> (); 
			Chessmans [x, y].SetPosition (x, y);
			activeChessman.Add (gmObjct);
			//print(onlineMultiplayer);
		}

	}

	//Get required position to set a chessman
	public Vector2 GetTileCenter(int x, int y){
		Vector2 origin = Vector2.zero;
		origin.x += (TILE_SIZE *x ) + TILE_OFFSET;
		origin.y += (TILE_SIZE *y ) + TILE_OFFSET;
		return origin;
	}

	//Spawn all chessman same time
	void SpawnAllChessMan(){
		if(FEN==null)
			FEN = "rkbqKbkr";
		char[] charFen = FEN.ToCharArray();
		List<int> rooksList = new List<int>();
		int kingPos = 0;
		for(int i=0;i<8;i++){
			switch (charFen[i]){
			case 'r' :
				//rook white
				SpawnChessMan(8,i,7);
				//SpawnChessMan(8,7,7);
				//rook
				SpawnChessMan(2,i,0);
				//SpawnChessMan(2,7,0);
				rooksList.Add(i);
				break;
			
			case 'k' :
				//knight
				SpawnChessMan(4,i,0);
				//SpawnChessMan(4,6,0);
				//knight
				SpawnChessMan(10,i,7);
				//SpawnChessMan(10,6,7);

				break;
			case 'b' :
				//bishop
				SpawnChessMan(3,i,0);
				//SpawnChessMan(3,5,0);
				//bishop
				SpawnChessMan(9,i,7);
				//SpawnChessMan(9,5,7);
				break;
			
			case 'K' :
				//King
				SpawnChessMan(0,i,0);
				//King
				SpawnChessMan(6,i,7);
				kingPos = i;
				break;

			case 'q' :
				//Queen
				SpawnChessMan(1,i,0);
				//Queen
				SpawnChessMan(7,i,7);
				break;
		

			}
		}
	

		//Pawn
		for (int i=0;i<8;i++)
			SpawnChessMan(5,i,1);

		//spawn Black team!


		//Pawn
		for (int i=0;i<8;i++)
			SpawnChessMan(11,i,6);

		int[] rooksInt = rooksList.ToArray();

		if(playerColor=="white"){
			leftRookWhite = Chessmans[rooksInt[0],0];
			rightRookWhite = Chessmans[rooksInt[1],0];
			leftRookBlack = Chessmans[rooksInt[1],7];
			rightRookBlack = Chessmans[rooksInt[0],7];
		}
		else {
			leftRookWhite = Chessmans[rooksInt[0],0];
			rightRookWhite = Chessmans[rooksInt[1],0];
			leftRookBlack = Chessmans[rooksInt[1],7];
			rightRookBlack = Chessmans[rooksInt[0],7];
		}

		whiteKing = Chessmans[kingPos,0];
		blackKing = Chessmans[kingPos,7];

		foreach(GameObject chessman in activeChessman){
			if(chessman.GetComponent<Chessman>().isWhite)
				activeChessmanWhite.Add(chessman);
			else activeChessmanBlack.Add(chessman);
		}

	}

	/// <summary>
	/// Checks for match draw.
	/// </summary>
	/// <returns><c>true</c>, if for match draw was checked, <c>false</c> otherwise.</returns>
	bool CheckForMatchDraw(){
		// only two kings standing
		if(activeChessmanBlack.Count==1 && activeChessmanWhite.Count==1){
			return true;
		}
		//single King against king+bishop or king+knight
		else if(activeChessmanBlack.Count==1 || activeChessmanWhite.Count==1){
			if(activeChessmanBlack.Count==1 && activeChessmanWhite.Count==2){
				foreach(GameObject go in activeChessmanWhite){
					if(go.GetComponent<Chessman>().GetType()==typeof(Knight) || go.GetComponent<Chessman>().GetType()==typeof(Bishop))
						return true;
				}
			}
			else if(activeChessmanWhite.Count==1 && activeChessmanBlack.Count==2){
				foreach(GameObject go in activeChessmanBlack){
					if(go.GetComponent<Chessman>().GetType()==typeof(Knight) || go.GetComponent<Chessman>().GetType()==typeof(Bishop))
						return true;
				}
			}
			
		}
		///king+bishop && king + bishop && bishops are of same color
		else if(activeChessmanBlack.Count==2 && activeChessmanWhite.Count==2){
			foreach(GameObject goW in activeChessmanWhite){
				foreach(GameObject goB in activeChessmanBlack){
					if(goW.GetComponent<Chessman>().GetType()==typeof(Bishop) && goB.GetComponent<Chessman>().GetType()==typeof(Bishop)){
						if(goW.GetComponent<Bishop>().isWhiteUnit==goB.GetComponent<Bishop>().isWhiteUnit)
							return true;
					} 
				}
			}
			
		}
		return false;
	}
	/// <summary>
	///  multiplayer[player=1,timeupWhite player=-1 timeupBlack] [single] player=11 white -11black
	/// </summary>
	/// <param name="player">Player.</param>
	public void EndGame(int player){ // player=1,timeupWhite player=-1 timeupBlack player




		if(onlineMultiplayer){
			gameLog.instance.gameover_Server();
			if(getUserID.win_by_disconnect_ins==1)
				notificationCheck_W.text = "YOU WON!!";
			else if(getUserID.win_by_disconnect_ins==0)
				notificationCheck_W.text = "Drawn!";
		} 

		else {

			switch(player)
			{
			case -11:
				whiteWins();
				break;
			case 11:
				blackWins();
				break;
			case 0:
				matchDrawn();
				break;


			}

			if (IsBlackCheck && IswhiteTeam) {
				notificationCheck_W.text = "YOU WON!!";

			} else if (isWhiteCheck && IswhiteTeam) {
				notificationCheck_W.text = "YOU LOSE!!";
			}

			if (isWhiteCheck && !IswhiteTeam) {
				notificationCheck_W.text = "YOU WON!!";

			} else if (IsBlackCheck && !IswhiteTeam) {
				notificationCheck_W.text = "YOU LOSE!!";
			}
		}



	}

	void EndGame2(){

		if (IsBlackCheck && IswhiteTeam) {
			notificationCheck_W.text = "YOU WON!!";
			if(onlineMultiplayer) gameLog.instance.gameover_Server();


		} else if (isWhiteCheck && IswhiteTeam) {
			notificationCheck_W.text = "YOU LOSE!!";
		}

		if (isWhiteCheck && !IswhiteTeam) {
			notificationCheck_W.text = "YOU WON!!";
			if(onlineMultiplayer) gameLog.instance.gameover_Server();

		} else if (IsBlackCheck && !IswhiteTeam) {
			notificationCheck_W.text = "YOU LOSE!!";
		}
	
	}

	void whiteWins(){
		whiteMaystillWin_checking();

		if(whitePlayerMayWin_boardmanager)
			notificationCheck_W.text = "White Wins";
		else notificationCheck_W.text = "Match Drawn";

	}
	void blackWins(){
		blackMaystillWin_checking();

		if(blackPlayerMayWin_boardmanager)
			notificationCheck_W.text = "Black Wins";
		else notificationCheck_W.text = "Match Drawn";
	}
	void matchDrawn(){
		notificationCheck_W.text = "Match Drawn";
	}

	bool thisMoveCreatesSelfCheck(){
		thiscreatesCheck = new bool[8,8];
		if(isWhiteTurn)
		{
			for(int i = 0;i<8;i++)
			{
				for(int j = 0;j<8;j++)
				{
					if(Chessmans[i,j]!=null && !Chessmans[i,j].isWhite)
					{
						thiscreatesCheck = Chessmans[i,j].PossibleMove();

						for(int k=0;k<8;k++){
							for (int l = 0;l<8;l++){
								if(thiscreatesCheck[k,l] && Chessmans[k,l]!=null && Chessmans[k,l].GetType() == typeof(King) && Chessmans[k,l].isWhite)
								{
									return true;

								}
							}
						}



					}
				}
			}
		}

		else if(!isWhiteTurn)
		{
			for(int i = 0;i<8;i++)
			{
				for(int j = 0;j<8;j++)
				{
					if(Chessmans[i,j]!=null && Chessmans[i,j].isWhite)
					{
						thiscreatesCheck = Chessmans[i,j].PossibleMove();

						for(int k=0;k<8;k++){
							for (int l = 0;l<8;l++){
								if(thiscreatesCheck[k,l] && Chessmans[k,l]!=null && Chessmans[k,l].GetType() == typeof(King) && !Chessmans[k,l].isWhite)
								{
									return true;

								}
							}
						}


					}
				}
			}
		}


		return false;
	}

	void thisIsCheckMate(){
		DefendCheck = new bool[8,8];
		Chessman tempChessman = null,tempCaptureChessman = null;
		//CheckMate for black team by white team	
		if(!isWhiteTurn)
		{	
			tempCheck = true;
			for(int i = 0;i<8;i++)
			{
				for(int j = 0;j<8;j++)
				{
					if(Chessmans[i,j]!=null && !Chessmans[i,j].isWhite && activeChessman.Contains(Chessmans[i,j].gameObject))
					{
						tempChessman = Chessmans[i,j];
						DefendCheck = tempChessman.PossibleMove();

						for(int k=0;k<8;k++){
							for (int l = 0;l<8;l++){
								if(DefendCheck[k,l] )
								{
									if(Chessmans[k,l]!=null){
										tempCaptureChessman = Chessmans[k,l];
										activeChessman.Remove(Chessmans[k,l].gameObject);
										Destroy(Chessmans[k,l].gameObject);
									}
									NormalMove(tempChessman,k,l);
									if(!thisMoveCreatesSelfCheck()){
										print(k.ToString()+l.ToString()+tempChessman.name+ "     " + tempChessman.PreviousX.ToString()+tempChessman.PreviousY.ToString());
										tempCheck = false;

									}

									GoToPreviousPos(tempChessman,k,l,tempCaptureChessman);


									//									if(!tempCheck)
									//										return false;

								}
								tempCaptureChessman = null;
							}
						}

						tempChessman = null;

					}
				}
			}
			if(!tempCheck)
				checkmate = false;	
			else checkmate = true;
		}


		//Checkmate for white team by black team
		else 	if(isWhiteTurn)
		{	
			tempCheck = true;
			for(int i = 0;i<8;i++)
			{
				for(int j = 0;j<8;j++)
				{
					if(Chessmans[i,j]!=null && Chessmans[i,j].isWhite && activeChessman.Contains(Chessmans[i,j].gameObject))
					{
						tempChessman = Chessmans[i,j];
						DefendCheck = tempChessman.PossibleMove();

						for(int k=0;k<8;k++){
							for (int l = 0;l<8;l++){
								if(DefendCheck[k,l] )
								{
									if(Chessmans[k,l]!=null){
										tempCaptureChessman = Chessmans[k,l];
										activeChessman.Remove(Chessmans[k,l].gameObject);
										Destroy(Chessmans[k,l].gameObject);

									}
									NormalMove(tempChessman,k,l);
									if(!thisMoveCreatesSelfCheck()){
										print(k.ToString()+l.ToString()+tempChessman.name+ "     " + tempChessman.PreviousX.ToString()+tempChessman.PreviousY.ToString());
										tempCheck = false;

									}

									GoToPreviousPos(tempChessman,k,l,tempCaptureChessman);


									//									if(!tempCheck)
									//										return false;

								}
								tempCaptureChessman = null;
							}
						}

						tempChessman = null;

					}
				}
			}
			if(!tempCheck)
				checkmate = false;	
			else checkmate = true;
		}


		//		return true;
	}

	public void restart(){
		SceneManager.LoadScene("2");
		//stopwatch.resetTime();
	}

	public void Fiscer960SetUp(){
		List<int> freeSpaceWhite = new List<int>();
		List<int> freeSpaceBlack = new List<int>();

		foreach (GameObject g in activeChessman)
			Destroy(g);
		BoardHighLight.Instance.HideHighLight();
		isWhiteTurn = true;


		for(int i=0;i<7;i=i+2)
			freeSpaceBlack.Add(i);
		for(int i=1;i<8;i=i+2)
			freeSpaceWhite.Add(i);

		int rook1,rook2,b1,b2,king;
		rook1 = Random.Range(0,8);
		if(freeSpaceBlack.Contains(rook1))
			freeSpaceBlack.Remove(rook1);
		else if(freeSpaceWhite.Contains(rook1))
			freeSpaceWhite.Remove(rook1);


		//Spawning single rook
		SpawnChessMan(2,rook1,0);
		SpawnChessMan(8,rook1,7);

		//Spawning Second rook
		rook2 = rook1;
		while (Mathf.Abs(rook1-rook2)<=1)
			rook2 = Random.Range(0,8);

		if(freeSpaceBlack.Contains(rook2))
			freeSpaceBlack.Remove(rook2);
		else if(freeSpaceWhite.Contains(rook2))
			freeSpaceWhite.Remove(rook2);

		SpawnChessMan(2,rook2,0);
		SpawnChessMan(8,rook2,7);

		//Spawn King
		king = Random.Range(1,(Mathf.Abs(rook1-rook2)-1));
		if(rook1>rook2){
			SpawnChessMan(0,rook2+king,0);
			SpawnChessMan(6,rook2+king,7);

			leftRookWhite = Chessmans[rook2,0];
			leftRookBlack=Chessmans[rook2,7];
			rightRookWhite = Chessmans[rook1,0];
			rightRookBlack = Chessmans[rook1,7];
			whiteKing = Chessmans[rook2+king,0];
			blackKing = Chessmans[rook2+king,7];

			if(freeSpaceBlack.Contains(rook2+king))
				freeSpaceBlack.Remove(rook2+king);
			else if(freeSpaceWhite.Contains(rook2+king))
				freeSpaceWhite.Remove(rook2+king);
		}
		else {
			SpawnChessMan(0,rook1+king,0);
			SpawnChessMan(6,rook1+king,7);

			leftRookWhite = Chessmans[rook1,0];
			leftRookBlack=Chessmans[rook1,7];
			rightRookWhite = Chessmans[rook2,0];
			rightRookBlack = Chessmans[rook2,7];
			whiteKing = Chessmans[rook1+king,0];
			blackKing = Chessmans[rook1+king,7];

			if(freeSpaceBlack.Contains(rook1+king))
				freeSpaceBlack.Remove(rook1+king);
			else if(freeSpaceWhite.Contains(rook1+king))
				freeSpaceWhite.Remove(rook1+king);
		}



		//Respawn Bishop
		int[] BW = freeSpaceWhite.ToArray();
		int[] BB = freeSpaceBlack.ToArray();
		b1 = Random.Range(0,freeSpaceWhite.Count);
		b2 =Random.Range(0,freeSpaceBlack.Count);

		SpawnChessMan(3,BW[b1],0);
		SpawnChessMan(9,BW[b1],7);
		SpawnChessMan(3,BB[b2],0);
		SpawnChessMan(9,BB[b2],7);
		freeSpaceWhite.Remove(BW[b1]);
		freeSpaceBlack.Remove(BB[b2]);

		//Respawn The REst
		freeSpaceWhite.AddRange(freeSpaceBlack);

		int[] rest3 = freeSpaceWhite.ToArray();
		//Spawn knight
		SpawnChessMan(4,rest3[0],0);
		SpawnChessMan(10,rest3[0],7);
		SpawnChessMan(4,rest3[2],0);
		SpawnChessMan(10,rest3[2],7);

		//Spawn Queen
		SpawnChessMan(1,rest3[1],0);
		SpawnChessMan(7,rest3[1],7);

		//Spawn white Pawns
		for (int i=0;i<8;i++)
			SpawnChessMan(5,i,1);
		//Black Pawns
		for (int i=0;i<8;i++)
			SpawnChessMan(11,i,6);

		freeSpaceBlack.Clear();
		freeSpaceWhite.Clear();
	}

	void MakeNotCastleAble(){
		if(SelectedChessMan == whiteKing)
		{
			whiteKing.gameObject.GetComponent<King>().castleAbleLeft = false;
			whiteKing.gameObject.GetComponent<King>().castleAbleRight = false;
		}

		if(SelectedChessMan == leftRookWhite)
		{
			whiteKing.gameObject.GetComponent<King>().castleAbleLeft = false;
		}
		if(SelectedChessMan== rightRookWhite)
		{
			whiteKing.gameObject.GetComponent<King>().castleAbleRight = false;
		}

		if(SelectedChessMan== blackKing)
		{

			blackKing.gameObject.GetComponent<King>().castleAbleLeft = false;
			blackKing.gameObject.GetComponent<King>().castleAbleRight = false;
		}
		if(SelectedChessMan == leftRookBlack)
		{
			blackKing.gameObject.GetComponent<King>().castleAbleLeft = false;
		}
		if(SelectedChessMan== rightRookBlack)
		{
			blackKing.gameObject.GetComponent<King>().castleAbleRight = false;
		}

	}

	bool  thereIsNoChessmanOrCheckBetweenWhiteTeamLeft(Chessman king,int yVal){
		//Initial return false if there are other chessmans 
		if (king.CurrentX!=1 && king.CurrentX!=2) {
			for (int i = 2; i < king.CurrentX; i++) {
				if (Chessmans [i, yVal] != null) {
					if( (Chessmans [i, yVal])== leftRookWhite)
						continue;
					else
			
					return false;
			
				}
			
			}
			 // Second time check for checks 
			for (int i =2; i < king.CurrentX; i++) {
				if (Chessmans [i, yVal] == null) {
			
					NormalMove (king, i, yVal);
					bool tempCheck = true;
					if (thisMoveCreatesSelfCheck ()) {
						print (king.CurrentX);
						print ("This creates self check");
						tempCheck = false;
					}
			
					GoToPreviousPos (king, i, yVal, null);
					if (!tempCheck)
						return false;	
				}
			
			}
		}

		else if (king.CurrentX==1) {
			for (int i = 2; i <= 3; i++) {
				if (Chessmans [i, yVal] != null) {
					
						return false;

				}

			}
			// Second time check for checks 
			for (int i =1; i <=2; i++) {
				if (Chessmans [i, yVal] == null) {

					NormalMove (king, i, yVal);
					bool tempCheck = true;
					if (thisMoveCreatesSelfCheck ()) {
						print (king.CurrentX);
						print ("This creates self check");
						tempCheck = false;
					}

					GoToPreviousPos (king, i, yVal, null);
					if (!tempCheck)
						return false;	
				}

			}
		}
		else if(king.CurrentX ==2){
			for (int i = leftRookWhite.CurrentX+1; i <= 3; i++) {
				if (Chessmans [i, yVal] != null) {
					if( (Chessmans [i, yVal])== whiteKing)
						continue;
					else

						return false;
				}
			}
			// Second time check for checks 

		}

		return true;


	}
	bool  thereIsNoChessmanOrCheckBetweenWhiteTeamRight(Chessman king,int yVal){

		if (king.CurrentX!=6) {
			for (int i = 6; i > king.CurrentX; i--) {
				if (Chessmans [i, yVal] != null) {
					if( (Chessmans [i, yVal])== rightRookWhite)
						continue;
					else

						return false;

				}

			}

			for (int i =6; i > king.CurrentX; i--) {
				if (Chessmans [i, yVal] == null) {

					NormalMove (king, i, yVal);
					bool tempCheck = true;
					if (thisMoveCreatesSelfCheck ()) {
						print (king.CurrentX);
						print ("This creates self check");
						tempCheck = false;
					}

					GoToPreviousPos (king, i, yVal, null);
					if (!tempCheck)
						return false;	
				}

			}
		}

		else if (king.CurrentX==6) {
			if (Chessmans[5,yVal]!=null || isWhiteCheck)
				return false;

		}

		return true;


	}
	bool  thereIsNoChessmanOrCheckBetweenBlackTeamLeft(Chessman king,int yVal){

		//Initial return false if there are other chessmans 
		if (king.CurrentX!=1 && king.CurrentX!=2) {
			for (int i = 2; i < king.CurrentX; i++) {
				if (Chessmans [i, yVal] != null) {
					if( (Chessmans [i, yVal])== rightRookBlack)
						continue;
					else

						return false;

				}

			}
			// Second time check for checks 
			for (int i =2; i < king.CurrentX; i++) {
				if (Chessmans [i, yVal] == null) {

					NormalMove (king, i, yVal);
					bool tempCheck = true;
					if (thisMoveCreatesSelfCheck ()) {
						print (king.CurrentX);
						print ("This creates self check");
						tempCheck = false;
					}

					GoToPreviousPos (king, i, yVal, null);
					if (!tempCheck)
						return false;	
				}

			}
		}

		else if (king.CurrentX==1) {
			for (int i = 2; i <= 3; i++) {
				if (Chessmans [i, yVal] != null) {

					return false;

				}

			}
			// Second time check for checks 
			for (int i =1; i <=2; i++) {
				if (Chessmans [i, yVal] == null) {

					NormalMove (king, i, yVal);
					bool tempCheck = true;
					if (thisMoveCreatesSelfCheck ()) {
						print (king.CurrentX);
						print ("This creates self check");
						tempCheck = false;
					}

					GoToPreviousPos (king, i, yVal, null);
					if (!tempCheck)
						return false;	
				}

			}
		}
		else if(king.CurrentX ==2){
			for (int i = rightRookBlack.CurrentX+1; i <= 3; i++) {
				if (Chessmans [i, yVal] != null) {
					if( (Chessmans [i, yVal])== blackKing)
						continue;
					else

						return false;
				}
			}
			// Second time check for checks 


		}

		return true;
	}
	bool  thereIsNoChessmanOrCheckBetweenBlackTeamRight(Chessman king,int yVal){

		if (king.CurrentX!=6) {
			for (int i = 6; i > king.CurrentX; i--) {
				if (Chessmans [i, yVal] != null) {
					if( (Chessmans [i, yVal])== leftRookBlack)
						continue;
					else

						return false;

				}

			}

			for (int i =6; i > king.CurrentX; i--) {
				if (Chessmans [i, yVal] == null) {

					NormalMove (king, i, yVal);
					bool tempCheck = true;
					if (thisMoveCreatesSelfCheck ()) {
						print (king.CurrentX);
						print ("This creates self check");
						tempCheck = false;
					}

					GoToPreviousPos (king, i, yVal, null);
					if (!tempCheck)
						return false;	
				}

			}
		}

		else if (king.CurrentX==6) {
			if (Chessmans[5,yVal]!=null || IsBlackCheck)
				return false;

		}

		return true;


	}

	public void LeftCastlingWhiteButton(){
		NormalMove(whiteKing,2,0);
		NormalMove(leftRookWhite,3,0);
		whiteKing.transform.position = GetTileCenter(2,0);
		leftRookWhite.transform.position = GetTileCenter(3,0);
		BoardHighLight.Instance.HideHighLight();
		isWhiteTurn=!isWhiteTurn;
		SelectedChessMan=null;
	}
	public void RightCastlingWhiteButton(){
		NormalMove(whiteKing,6,0);
		NormalMove(rightRookWhite,5,0);
		whiteKing.transform.position = GetTileCenter(6,0);
		rightRookWhite.transform.position = GetTileCenter(5,0);
		BoardHighLight.Instance.HideHighLight();
		isWhiteTurn=!isWhiteTurn;
		SelectedChessMan=null;
	}
	public void LeftCastlingBlackButton(){
		NormalMove(blackKing,2,7);
		NormalMove(rightRookBlack,3,7);
		blackKing.transform.position = GetTileCenter(2,7);
		rightRookBlack.transform.position = GetTileCenter(3,7);
		BoardHighLight.Instance.HideHighLight();
		isWhiteTurn=!isWhiteTurn;
		SelectedChessMan=null;
	}
	public void RightCastlingBlackButton(){
		NormalMove(blackKing,6,7);
		NormalMove(leftRookBlack,5,7);
		blackKing.transform.position = GetTileCenter(6,7);
		leftRookBlack.transform.position = GetTileCenter(5,7);
		BoardHighLight.Instance.HideHighLight();
		isWhiteTurn=!isWhiteTurn;
		SelectedChessMan=null;
	}

	void MoveRookWhileCAstling(int x,int y){
		if(SelectedChessMan.GetType() == typeof(King))
		{
			if((x-whiteKing.PreviousX)<0 && y==0 && SelectedChessMan.isWhite && castleAbleW_L && whiteKing.GetComponent<King>().castleAbleLeft && !BoardHighLight.Instance.castleButtonWhiteLeft.activeSelf)
			{
				NormalMove(leftRookWhite,whiteKing.CurrentX+1,0);
				leftRookWhite.gameObject.transform.position = GetTileCenter(3,0);

			}
			else if((x-whiteKing.PreviousX)>0 && y==0 && SelectedChessMan.isWhite && castleAbleW_R && whiteKing.GetComponent<King>().castleAbleRight && !BoardHighLight.Instance.castleButtonWhiteRight.activeSelf)
			{
				NormalMove(rightRookWhite,whiteKing.CurrentX-1,0);
				rightRookWhite.gameObject.transform.position = GetTileCenter(5,0);

			}

			if((x-blackKing.PreviousX)<0 &&y==7 && !SelectedChessMan.isWhite && castleableB_L && blackKing.GetComponent<King>().castleAbleLeft && !BoardHighLight.Instance.castleButtonBlackLeft.activeSelf)
			{
				NormalMove(rightRookBlack,blackKing.CurrentX+1,7);
				rightRookBlack.gameObject.transform.position = GetTileCenter(3,7);

			}
			else if((x-blackKing.PreviousX)>0 && y==7 && !SelectedChessMan.isWhite && castleableB_R && blackKing.GetComponent<King>().castleAbleRight && !BoardHighLight.Instance.castleButtonBlackRight.activeSelf)
			{
				NormalMove(leftRookBlack,blackKing.CurrentX-1,7);
				leftRookBlack.gameObject.transform.position = GetTileCenter(5,7);


			}
		}
	}
	/// <summary>
	/// check if white will win or will draw
	/// </summary>
	private void whiteMaystillWin_checking(){
		
		int bishopNum=0;
		foreach (GameObject go in activeChessmanWhite)
		{
			if(go.GetComponent<Chessman>().GetType()==typeof(Pawn)||go.GetComponent<Chessman>().GetType()==typeof(Queen)||go.GetComponent<Chessman>().GetType()==typeof(Rook)){
				whitePlayerMayWin_boardmanager=true;
				return;
			}
			if(go.GetComponent<Chessman>().GetType()==typeof(Bishop)){
				bishopNum++;
			}

		}
		if(bishopNum>1) 
		{
			whitePlayerMayWin_boardmanager=true;
			return;
		}
		whitePlayerMayWin_boardmanager=false;
	}

	/// <summary>
	/// check if black can win or will draw
	/// </summary>
	private void blackMaystillWin_checking(){
		
		int bishopNum=0;
		foreach (GameObject go in activeChessmanBlack)
		{
			if(go.GetComponent<Chessman>().GetType()==typeof(Pawn)||go.GetComponent<Chessman>().GetType()==typeof(Queen)||go.GetComponent<Chessman>().GetType()==typeof(Rook)){
				blackPlayerMayWin_boardmanager=true;
				return;
			}
			if(go.GetComponent<Chessman>().GetType()==typeof(Bishop)){
				bishopNum++;
			}

		}
		if(bishopNum>1) {
			blackPlayerMayWin_boardmanager=true;
			return;
		}
		blackPlayerMayWin_boardmanager=false;
	}

	public void GoToHomePage(){
		SceneManager.LoadScene("MAIN");
	}

	void RotateBoard(){
		if(onlineMultiplayer){

			playerColor = gameLog.instance.playerColor;


		}

		if(playerColor == "black"){
			Camera_.transform.rotation = Quaternion.Euler(new Vector3(0,0,180));

		}
		SpawnAllChessMan();
	}
}
