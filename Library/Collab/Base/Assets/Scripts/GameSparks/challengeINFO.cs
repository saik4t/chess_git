﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSparks.Core;
using GameSparks.Api;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using UnityEngine.SceneManagement;
public class challengeINFO : MonoBehaviour {

	public static string opponentID;
	public static string opponentName;
	public static string ownID;
	public static string challengeID;
	public static string turn;

	public Text chllengerName, opponentRank;

	// Use this for initialization
	void Start () {

		ownID = authentication.OWNID_main;

//		new AccountDetailsRequest()
//			.Send((response) => {
//				ownID = response.UserId;
//				//gameLog.instance.ownId_temp = ownID;
//				//pass ownid
//				Debug.Log("set own id : "+ ownID);
//				turn = ownID;
//
//			});
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void setChallangeID(string id){
		challengeID = id;
	}

	public void setOpponentID(string o_id)
	{
		opponentID = o_id;
	}


	public void AcceptChallengeBtn_startgame(){


		string challengeid_toaccept = challengeID;

		//pass accepted challenge ID
		getUserID.challengeID_ins = challengeid_toaccept;
		getUserID.opponentID_ins = opponentID;

		new AcceptChallengeRequest()
			.SetChallengeInstanceId(challengeid_toaccept)
			.SetMessage("oka lets play")
			.Send((response2) => {
				if(!response2.HasErrors){
					string challengeInstanceId = response2.ChallengeInstanceId; 
					challengeID = challengeInstanceId;
					Debug.Log("MATCH started!!! " + "id : " + challengeInstanceId);
					GSData scriptData = response2.ScriptData;

					///the match will start now
					/// function to go to new screen
					SceneManager.LoadScene("2");
				}
				else{
					Debug.Log("game is not started");
				}
			});
		//challengeINFO.whosturn = false;
		//setIDs ();
		//InvokeRepeating ("checkData",1,1);

		//turn fixed
		gameLog.instance.turnFixer ();


	}


//	public void setIDs(){
//
//
//		new ListChallengeRequest().SetShortCode("chessChal")
//			.SetState("RUNNING") //We want to get all games that are running
//			.SetEntryCount(50) //We want to pull in the first 50
//			.Send((response) =>
//				{
//					foreach (var challenge in response.ChallengeInstances)
//					{
//
//						challengeID = challenge.ChallengeId;
//						opponentName = challenge.Challenger.Name;
//						opponentID = challenge.Challenger.Id;
//
//						Debug.Log ("challenge details and ids : "+ challengeID + opponentName + opponentID);
//						//turn = challenge.ScriptData.GetString("turn");
//						Debug.Log("match detais stored!!");
//
//
//					}
//				});
//
//		//Debug.Log ("challenge details and ids : "+ challengeID + opponentName + opponentID);
//
//	}
}
