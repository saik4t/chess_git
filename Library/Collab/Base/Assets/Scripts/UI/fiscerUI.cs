﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class fiscerUI : MonoBehaviour {
	public Image fiscer,standard;
	
	// Use this for initialization
	void Start () {
		
	}

	void Update(){
		if(BoardManager.fiscer360){
			fiscer.color = new Color32(118,101,83,255);
			standard.color = new Color32(174,128,78,255);
		}
		else {
			fiscer.color = new Color32(174,128,78,255);
			standard.color = new Color32(118,101,83,255);
		}
	}
	
	public void FiscerClicked(){
		BoardManager.fiscer360 = true;

	}
	public void StandardClicked(){
		BoardManager.fiscer360 = false;
	}

}
