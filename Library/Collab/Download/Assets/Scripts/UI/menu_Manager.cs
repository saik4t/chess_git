﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menu_Manager : MonoBehaviour {
	public GameObject timePanel,leaderPanel,SettingPanel,allContent,navPanel,profilePanel;
	Vector3 primaryPosLeader,navPrimaryPos,primaryPosSettings,primaryPosProfile,primaryPosTime;
	// Use this for initialization
	void Start () {
		timePanel.SetActive(true);
		primaryPosTime = timePanel.transform.position;
		primaryPosLeader = leaderPanel.transform.position;
		navPrimaryPos = navPanel.transform.position;
		primaryPosSettings = SettingPanel.transform.position;
		primaryPosProfile = profilePanel.transform.position;
	
	}
	public void GrandMaster(){
		leaderPanel.transform.position = primaryPosLeader-new Vector3(Screen.width,0);
		allContent.GetComponent<CanvasGroup>().alpha = 0;
	}
	public void TimePanel(){
		//SceneManager.LoadScene("2");
		BoardManager.onlineMultiplayer = true;
		timePanel.transform.position = primaryPosTime - new Vector3(4*Screen.width,0);
		allContent.GetComponent<CanvasGroup>().alpha = 1;
	}
	public void Settings(){
		SettingPanel.transform.position = primaryPosSettings-new Vector3(2*Screen.width,0);
		allContent.GetComponent<CanvasGroup>().alpha = 0;
	}
	public void Profile(){
		profilePanel.transform.position = primaryPosProfile-new Vector3(3*Screen.width,0);
		allContent.GetComponent<CanvasGroup>().alpha = 0;
	}
	public void orginalScreen(){
		leaderPanel.transform.position = primaryPosLeader;
		SettingPanel.transform.position = primaryPosSettings;
		profilePanel.transform.position = primaryPosProfile;
		timePanel.transform.position = primaryPosTime;
		allContent.GetComponent<CanvasGroup>().alpha = 1;

	}
	public void offLinePlayWithPeople(){
		//SceneManager.LoadScene("2");
		BoardManager.onlineMultiplayer = false;
		timePanel.transform.position = primaryPosTime - new Vector3(4*Screen.width,0);
		allContent.GetComponent<CanvasGroup>().alpha = 1;
	}

	public void min_1(){
		//timePanel.SetActive(false);
		SceneManager.LoadScene("2");
		stopwatch.minChoosen=1;
	}
	public void min_2(){
		//timePanel.SetActive(false);
		SceneManager.LoadScene("2");
		stopwatch.minChoosen=2;
	}
	public void min_5(){
		//timePanel.SetActive(false);
		SceneManager.LoadScene("2");
		stopwatch.minChoosen=5;
	}
	public void min_10(){
		//timePanel.SetActive(false);
		SceneManager.LoadScene("2");
		stopwatch.minChoosen=10;
	}
	public void min_20(){
		//timePanel.SetActive(false);
		if (!BoardManager.onlineMultiplayer) {
			SceneManager.LoadScene ("2");
		}
		stopwatch.minChoosen=20;
	}
	public void min_30(){
		//timePanel.SetActive(false);
		SceneManager.LoadScene("2");
		stopwatch.minChoosen=30;
	}
	public void min_60(){
		//timePanel.SetActive(false);
		SceneManager.LoadScene("2");
		stopwatch.minChoosen=60;
	}
}
