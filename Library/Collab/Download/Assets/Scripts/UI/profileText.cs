﻿using UnityEngine.UI;
using UnityEngine;
using System;
using GameSparks.Core;
using GameSparks.Api;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;

public class profileText : MonoBehaviour {
	public Text displayName,Rating,Points,Wins,Draws,Losses,WinP,DrawP,LossP;
	public string Country;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	public void GetOwnProfile () {

		displayName.text = getUserID.displayName_ins;
		Rating.text  = getUserID.rank_ins.ToString();
		Points.text = getUserID.points_ins.ToString();
		Wins.text = getUserID.matchWon_ins.ToString();
		Draws.text = getUserID.matchDrawn_ins.ToString();
		Losses.text = (getUserID.matchPlayed_ins - getUserID.matchWon_ins-getUserID.matchDrawn_ins).ToString();
		float winp = ((float)getUserID.matchWon_ins/(float)getUserID.matchPlayed_ins)*100;
		float drawp = ((float)getUserID.matchDrawn_ins/(float)getUserID.matchPlayed_ins)*100;
		float lossp = 100-winp-drawp;
		WinP.text = Math.Round(winp,2).ToString()+" %";
		DrawP.text = Math.Round(drawp,2).ToString()+ " %";
		LossP.text =  Math.Round(lossp,2).ToString()+ " %";
		
	}


	public void GetOpponentProfile(){


		new GameSparks.Api.Requests.LogEventRequest()
			.SetEventKey("onlinePlayerList")
			.Send((response) => {
				if (!response.HasErrors) {

					var data = response.ScriptData.GetGSDataList("onlinePlayer_Data");
					foreach(var d in data.ToArray()){
						if(d.GetString("playerId") == getUserID.opponentID_ins){

							displayName.text = d.GetString("displayName");
							Country = d.GetString("playerCountry").ToString();
							Rating.text = d.GetInt("playerRank").ToString();
							Points.text = d.GetInt("playerCountry").ToString();
							int Played_ = (int) d.GetInt("playedMatch");
							int Wins_= (int) d.GetInt("wonMatch");
							int Draws_ = (int) d.GetInt("drawnMatch");
							int Losses_ = Played_ - Wins_ - Draws_;

							Wins.text = Wins_.ToString();
							Draws.text = Draws_.ToString();
							Losses.text = Losses_.ToString();
							float winp = ((float)Wins_/(float)Played_)*100;
							float drawp = ((float)Draws_/(float)Played_)*100;
							float lossp = 100-winp-drawp;
							WinP.text = Math.Round(winp,2).ToString()+" %";
							DrawP.text = Math.Round(drawp,2).ToString()+ " %";
							LossP.text =  Math.Round(lossp,2).ToString()+ " %";


						}
					}

				} else {


					Debug.Log("Error Loading online Player Profile");
				}


			});
		


	}

}
