﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class testButtonListView : MonoBehaviour {
	public GameObject prefab,panel;
	float distance;
	public float spacing = 10f,topPadding = 10,leftPadding = -1000; 
	// Use this for initialization
	void Start () {
		distance =  prefab.GetComponent<RectTransform>().sizeDelta.y;

	}
	
	// Update is called once per frame
	void Update () {
		if(panel.transform.childCount!=0){
			panel.GetComponent<RectTransform>().sizeDelta = new Vector2(15,	distance+(panel.transform.childCount-1)*(distance+spacing));
		for(int i=0;i<panel.transform.childCount;i++){
			
			panel.transform.GetChild(i).GetComponent<RectTransform>().position = new Vector3(panel.transform.parent.GetComponent<RectTransform>().position.x,panel.transform.GetChild(0).GetComponent<RectTransform>().position.y - (distance+spacing)*i,0 );
		}
		}
		else {
			panel.GetComponent<RectTransform>().sizeDelta = new Vector2(15,	distance);
		}
	}
	public void onClick(){
		GameObject go = Instantiate(prefab,panel.transform);
		//print( panel.transform.childCount);
	}
}
