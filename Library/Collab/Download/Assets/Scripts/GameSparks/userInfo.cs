﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSparks.Core;
using GameSparks.Api;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using UnityEngine.SceneManagement;
using TMPro;

public class userInfo : MonoBehaviour {
	public TextMeshProUGUI displayNmaeT,playerRankT;
	public List<string> userIds = new List<string> {""};
	public static string challegeID_challenged;

	public string ownID_inUserinfoScript;
	public string ChallengedGameNextTurn;

	public string fetched_Online_UserId;


	public string UserID;

	void Start(){
		ownID_inUserinfoScript = challengeINFO.ownID;
		displayNmaeT = GetComponent<TextMeshProUGUI>();
		playerRankT = GetComponent<TextMeshProUGUI>();

	}

	public void setID(string id){
		UserID = id;
	}


	public void SetName_Rank(string name,string rank){
		displayNmaeT.SetText(name);
		playerRankT.SetText(rank);
	}


	public void ChallengeBtn(){

		BoardManager.onlineMultiplayer = true;

		getUserID.am_I_a_Challenger_ins = true;

		userIds.Clear();
		userIds.Add(UserID);

		getUserID.opponentID_ins = UserID ;

		Debug.Log ("NOW : " + System.DateTime.Now);
		Debug.Log ("Date : " + System.DateTime.Now.AddDays(1) + " Time : " + System.DateTime.Now.AddMinutes(15));

//		new CreateChallengeRequest()
//			.SetAccessType("PRIVATE")
//			.SetChallengeMessage(getUserID.FEN_sample_ins)
//			.SetChallengeShortCode("chessChal")
//			.SetEndTime(System.DateTime.Now.AddMinutes(15)) //We set a date and time the challenge will end on
//			.SetMaxPlayers(2)
//			.SetMinPlayers(0)
//			.SetSilent(false)
//			.SetUsersToChallenge(userIds)
//			.Send((response) => {
//				if(!response.HasErrors){
//					string challengeInstanceId = response.ChallengeInstanceId; 
//					GSData scriptData = response.ScriptData; 
//					//challegeID = challengeInstanceId;
//					Debug.Log("challenged done. Cid: " + challengeInstanceId);
//					challegeID_challenged = challengeInstanceId;
//
//					//go to the chess board screeen and wait for 50sec for accepting challenge and start game
//					//function here
//
//					getUserID.challengeID_ins = challegeID_challenged;
//					//setting global challengeID
//
//					new LogEventRequest()
//						.SetEventKey("UpdateChellengeID")
//						.SetEventAttribute("challenge_id_", challegeID_challenged)
//						.Send((response2) =>
//							{
//								if (!response2.HasErrors)
//								{
//									Debug.Log("challengeID Data sent - Done");
//								}
//								else
//								{
//									Debug.Log("Failed");
//								}
//							});
//
//					////start the loading scene
//					//SceneManager.LoadScene ("4");
//					SceneManager.LoadScene ("2");
//
//
//				}
//				else{
//					Debug.Log("can not challenge user");
//				}
//			});

		//whosturn = true;
		//to define who will play the turn first
		menu_Manager canvas = GameObject.FindObjectOfType<menu_Manager>();
		canvas.TimePanel ();
	}



	public void setTime(){



	}




}
