﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using GameSparks.Api;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.SceneManagement;

public class StartChallengeEvent : MonoBehaviour {

	public string ownID_inUserinfoScript;
	public string ChallengedGameNextTurn;
	public static string challengeID;
	public string ChallengeState;

	float timeLeft = 60.0f;
	public Text countText;

	// Use this for initialization
	void Start () {

		challengeID = userInfo.challegeID_challenged;

		
		//checking if match started and turn fixing
		//InvokeRepeating("isGameStarted",0,5);

	}
	
	// Update is called once per frame
	void Update () {


		GameSparks.Api.Messages.ChallengeStartedMessage.Listener = (message) => {
			var challenge = message.Challenge; 
			string messageId = message.MessageId; 
			bool? notification = message.Notification;

			if(challenge.NextPlayer == getUserID.ownId_ins)
				BoardManager.playerColor = "white";
			else BoardManager.playerColor = "balck";

			getUserID.isPlaying_ins = 1;

			SceneManager.LoadScene ("2");

		};

//		if (ChallengeState == "RUNNING") {
//
//			//start the match scene
//			SceneManager.LoadScene ("2");
//			//start here
//
//			CancelInvoke ("isGameStarted");
//
//		}

		//play the timer if not in game only
	//	if (getUserID.isPlaying_ins == 0) {
			//start timer
			timeLeft -= Time.deltaTime;
			countText.text = Mathf.Round (timeLeft).ToString ();
			if (timeLeft <= 0) {
				SceneManager.LoadScene ("MAIN");
				withdrawChallenge ();

			}
		//}
		
	}

	public void withdrawChallenge(){

		new WithdrawChallengeRequest()
			.SetChallengeInstanceId(challengeID)
			.SetMessage("time over")
			.Send((response) => {
				string challengeInstanceId = response.ChallengeInstanceId; 
				GSData scriptData = response.ScriptData; 
			});
	
	
	}

	public void gotoMainWindow(){
		SceneManager.LoadScene ("MAIN");
	}

	public void isGameStarted(){

		new GetChallengeRequest()
			.SetChallengeInstanceId(challengeID)
			.Send((response) => {
				var challenge = response.Challenge; 
				ChallengeState = challenge.State;
			});
		
	}
}
