﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using GameSparks.Api;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using UnityEngine.UI;
using System.Linq;


public class challengeManager : MonoBehaviour {

	public static bool whosturn; 

	public GameObject challengeButton;
	public GameObject panel;

	//extra
	public string challegeID = "";
	public List<string> challengeid_list1 = new List<string>();
	public string _Rating ;

	// Use this for initialization
	void Start () {

		ListAllChallenge();

		//TEMPauthUser();
		//InvokeRepeating("ListAllChallenge",1,10);
	}
	
	// Update is called once per frame
	void Update () {

		//It fires only when a new challenge is issued
		// need to list the challenge and compare with a local challenge list to add or remove from it to generate the UI
		// it will supress the glitch

		GameSparks.Api.Messages.ChallengeIssuedMessage.Listener = (message) => {

			getUserID.am_I_a_Challenger_ins = false;

			var challenge = message.Challenge; 
			string message2 = message.Message; 
			string who = message.Who; 

			Debug.Log("new challenge found : " + challenge.ChallengeId.ToString());
			//runs when new challenge issues
			//ListAllChallenge();


			//Set our variables
			challegeID = challenge.ChallengeId;
			challengeid_list1.Add(challenge.ChallengeId); 

// opponent rank should be retreived in another way

			string opponentRank_value = challenge.ChallengeMessage;
			string ChallengerName = challenge.Challenger.Name;
			string ChallengeID = challenge.ChallengeId;

			string opponentID = challenge.Challenger.Id;
			//need this in global data ??????????????????????????????????????????????????????????????????????????????????????????????????????????????????????

			//getUserID.challengeID_ins = ChallengeID;

			Debug.Log("c ids are: " + challenge.ChallengeId);
			Debug.Log("challenger : " + challenge.Challenger.Name);

			Debug.Log("challenge msg : " + opponentRank_value);

			getUserID.incomming_challenge_ins = challengeid_list1.Count;

		
			GameObject go = Instantiate(challengeButton,panel.transform);
			go.GetComponent<challengeINFO>().chllengerName.text = ChallengerName;
			go.GetComponent<challengeINFO>().setChallangeID(ChallengeID);
			go.GetComponent<challengeINFO>().setOpponentID(opponentID);
			go.GetComponent<challengeINFO>().opponentRank.text = opponentRank_value;

		};


		//if any challenge is withdrawn
		//have to remove from the list
		GameSparks.Api.Messages.ChallengeWithdrawnMessage.Listener = (message) => {
			var challenge = message.Challenge; 
			string message3 = message.Message; 
			string messageId = message.MessageId; 
			string title = message.Title; 
			string who = message.Who; 

			//have to change the list here then refresh the list
			Debug.Log("withdrawn id : " + challenge.ChallengeId.ToString());

			//runs when new challenge issues
			//ListAllChallenge();

			destroyChallenge(challenge.ChallengeId.ToString());

		};


		
	}

	public void destroyChallenge(string challengeIdToRemove){
		
		foreach(Transform g in transform){

			string ch_id_temp = g.gameObject.GetComponent<challengeINFO> ().ch_id_temp;

			if (challengeIdToRemove == ch_id_temp) {
				Destroy (g.gameObject);

			}

		}
	}


	public void ListAllChallenge(){

		new GameSparks.Api.Requests.ListChallengeRequest ().SetShortCode ("chessChal")
			.SetState ("RECEIVED")
			.SetEntryCount (50)
			.Send ((response) => {
				//	var challengeInstances = response.ChallengeInstances;


				foreach (var challenge in response.ChallengeInstances)
				{
					//Set our variables
					challegeID = challenge.ChallengeId;
					challengeid_list1.Add(challenge.ChallengeId); 

//					string opponentRank_value = challenge.ChallengeMessage;


					string ChallengerName = challenge.Challenger.Name;
					string ChallengeID = challenge.ChallengeId;

					string opponentID = challenge.Challenger.Id;
					string opponentRank_value = getOpponentDetails(opponentID);

					//need this in global data ??????????????????????????????????????????????????????????????????????????????????????????????????????????????????????

					//getUserID.challengeID_ins = ChallengeID;

					Debug.Log("c ids are: " + challenge.ChallengeId);
					Debug.Log("challenger : " + challenge.Challenger.Name);

					GameObject go = Instantiate(challengeButton,panel.transform);
					go.GetComponent<challengeINFO>().chllengerName.text = ChallengerName;
					go.GetComponent<challengeINFO>().setChallangeID(ChallengeID);
					go.GetComponent<challengeINFO>().setOpponentID(opponentID);
					go.GetComponent<challengeINFO>().opponentRank.text = opponentRank_value;
				}


			});
	}


	public void TEMPauthUser(){
		new GameSparks.Api.Requests.AuthenticationRequest()
			.SetPassword("pass")
			.SetUserName("zeem_pc")
			.Send((response) => {
				string authToken = response.AuthToken; 
				string displayName = response.DisplayName; 
				bool? newPlayer = response.NewPlayer; 
				GSData scriptData = response.ScriptData; 
				var switchSummary = response.SwitchSummary; 
				string userId = response.UserId; 

				Debug.Log("auth req done ============== "+"auth: "+authToken + "displayName: "+displayName + "userId: "+userId);
			});

	}


	public string getOpponentDetails(string playerID_fromList){

		new GameSparks.Api.Requests.LogEventRequest()
			.SetEventKey("onlinePlayerList")
			.Send((response) => {
				if (!response.HasErrors) {

					var data = response.ScriptData.GetGSDataList("onlinePlayer_Data");
					foreach(var d in data.ToArray()){
						if(d.GetString("playerId") == playerID_fromList){
							
							_Rating = d.GetInt("playerRank").ToString();

						}
					}

				} else {


					Debug.Log("Error Loading online Player Profile");
				}


			});

		return _Rating;

	}
}
