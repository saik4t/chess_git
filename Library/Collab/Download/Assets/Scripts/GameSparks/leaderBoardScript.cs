﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using GameSparks.Api;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using UnityEngine.UI;

public class leaderBoardScript : MonoBehaviour {
	private testButtonListView list;
	//public GameObject parent;

	//public Text text_leader; 
	public Text NameL,rankL,countryL;

	// Use this for initialization
	void Start () {
		list = GetComponent<testButtonListView>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void getLeaderBoardData(){
	
		new GameSparks.Api.Requests.LeaderboardDataRequest()
			.SetEntryCount(10)
			.SetLeaderboardShortCode("HIGH_SCORE_LB")
			.Send((response) => {

				//GSData scriptData = response.ScriptData.GetGSData("SCORE_ATTR");


				//text_leader.text = "";
				foreach (var leadData in response.Data)
				{
					
					string player_name = leadData.UserName;
					string player_rank = leadData.Rank.ToString();
					string player_country = leadData.Country;
					string player_id = leadData.UserId;

					string point = leadData.JSONData["SCORE_ATTR"].ToString();
					NameL.text = player_name;
					rankL.text = player_rank;
					//countryL.text = player_country;
					//var point = leadData.ScriptData.GetNumber("SCORE_ATTR");



					//text_leader.text = text_leader.text + "\n" + player_rank + " " + player_name + " " + point ;

					//Debug.Log("script data : "  + point.GetType());
					GameObject go = Instantiate(list.prefab,list.panel.transform);
					
				}


				//Debug.Log("script data : "  );
			});


	}
}
